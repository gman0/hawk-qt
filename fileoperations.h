#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

#include "fileoperation.h"
#include "iocontrol.h"
#include <QMainWindow>
#include <hawk/io/IO_task.h>
#include <list>
#include <memory>

namespace Ui {
class FileOperations;
}

using IOTask =
    std::pair<std::unique_ptr<hawk::IO_task>, std::unique_ptr<IOControl>>;

class FileOperations : public QMainWindow {
  Q_OBJECT

  Ui::FileOperations* ui;
  std::list<IOTask> m_tasks;

public:
  explicit FileOperations(QWidget* parent = nullptr);
  ~FileOperations();

  void add(FileOperation type, const QList<QUrl>& srcs, const hawk::Path& dst);

private slots:
  void handle_exists_error(hawk::IO_task::Target& t);
  void handler_error(hawk::Filesystem_error e, hawk::IO_task::Target* t,
                     hawk::IO_task* parent);

  void clear_finished();
};

#endif // FILEOPERATIONS_H
