#include "sort.h"
#include <cstring>
#include <hawk/Interrupt.h>

/// Sort_alpha

bool Sort_alpha::operator()(hawk::Static_idx a, hawk::Static_idx b)
{
  unsigned char type_a = st.type(a);
  unsigned char type_b = st.type(b);

  if (hawk::is_directory(type_a) && !hawk::is_directory(type_b))
    return true;
  if (!hawk::is_directory(type_a) && hawk::is_directory(type_b))
    return false;

  return strcasecmp(st.name(a), st.name(b)) < 1;
}

/// Sort_natural

Sort_natural::Sort_natural(const hawk::Path&,
                           hawk::Directory::Static static_access)
    : st{static_access}
{
  c.setCaseSensitivity(Qt::CaseInsensitive);
  c.setNumericMode(true);

  for (uint64_t i = 0; i < st.size(); i++) {
    hawk::soft_interruption_point();
    keys.push_back(c.sortKey(st.name(i)));
  }
}

bool Sort_natural::operator()(hawk::Static_idx a, hawk::Static_idx b)
{
  unsigned char type_a = st.type(a);
  unsigned char type_b = st.type(b);

  if (hawk::is_directory(type_a) && !hawk::is_directory(type_b))
    return true;
  if (!hawk::is_directory(type_a) && hawk::is_directory(type_b))
    return false;

  return keys[a.value()].compare(keys[b.value()]) < 1;
}

/// Sort_natural_reverse

Sort_natural_reverse::Sort_natural_reverse(
    const hawk::Path&, hawk::Directory::Static static_access)
    : st{static_access}
{
  c.setCaseSensitivity(Qt::CaseInsensitive);
  c.setNumericMode(true);

  for (uint64_t i = 0; i < st.size(); i++) {
    hawk::soft_interruption_point();
    keys.push_back(c.sortKey(st.name(i)));
  }
}

bool Sort_natural_reverse::operator()(hawk::Static_idx a, hawk::Static_idx b)
{
  unsigned char type_a = st.type(a);
  unsigned char type_b = st.type(b);

  if (hawk::is_directory(type_a) && !hawk::is_directory(type_b))
    return true;
  if (!hawk::is_directory(type_a) && hawk::is_directory(type_b))
    return false;

  return keys[b.value()].compare(keys[a.value()]) < 1;
}

/// Sort_size

Sort_size::Sort_size(const hawk::Path& dirpath,
                     hawk::Directory::Static static_access)
    : st{static_access}
{
  for (uint64_t i = 0; i < st.size(); i++) {
    hawk::soft_interruption_point();
    sizes.push_back(hawk::file_size(dirpath / st.name(i)));
  }
}

bool Sort_size::operator()(hawk::Static_idx a, hawk::Static_idx b)
{
  unsigned char type_a = st.type(a);
  unsigned char type_b = st.type(b);

  if (hawk::is_directory(type_a) && !hawk::is_directory(type_b))
    return true;
  if (!hawk::is_directory(type_a) && hawk::is_directory(type_b))
    return false;

  return sizes[a.value()] > sizes[b.value()];
}

Sort_size_reverse::Sort_size_reverse(const hawk::Path& dirpath,
                                     hawk::Directory::Static static_access)
    : st{static_access}
{
  for (uint64_t i = 0; i < st.size(); i++) {
    hawk::soft_interruption_point();
    sizes.push_back(hawk::file_size(dirpath / st.name(i)));
  }
}

bool Sort_size_reverse::operator()(hawk::Static_idx a, hawk::Static_idx b)
{
  unsigned char type_a = st.type(a);
  unsigned char type_b = st.type(b);

  if (hawk::is_directory(type_a) && !hawk::is_directory(type_b))
    return true;
  if (!hawk::is_directory(type_a) && hawk::is_directory(type_b))
    return false;

  return sizes[b.value()] > sizes[a.value()];
}
