import QtQuick 2.0
import hawk.process 1.0

Item {
    Process {
        id: proc
    }

    function openFile(filepath) {
        proc.start('xdg-open', [filepath])
    }
}
