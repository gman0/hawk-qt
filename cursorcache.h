#ifndef CURSORCACHE_H
#define CURSORCACHE_H

#include <QModelIndex>
#include <QObject>
#include <hawk/Path.h>
#include <map>
#include <mutex>

struct Cursor {
  int r;
  ino64_t i;
  hawk::Path f;

  Cursor() : r{-1}, i{0} {}
  Cursor(int row, ino64_t ino, const hawk::Path& filename)
      : r{row}, i{ino}, f{filename}
  {
  }

  inline int row() const { return r; }
  inline ino64_t inode() const { return i; }
  inline const hawk::Path& filename() const { return f; }
  inline bool is_valid() const { return r >= 0; }
};

class CursorCache : public QObject {
private:
  Q_OBJECT

  using Map = std::map<hawk::Path, Cursor>;
  Map m_cursors;
  mutable std::mutex m_mtx;

public:
  explicit CursorCache(QObject* parent = nullptr) : QObject{parent} {}
  CursorCache(const CursorCache& cursors);
  CursorCache& operator=(const CursorCache& cursors);

  std::pair<Cursor, bool> find(const hawk::Path& dirpath);

public slots:
  void store(const hawk::Path& dirpath, const Cursor& c);
  // Stores each node of the path
  void store(const hawk::Path& path);
};

#endif // CURSORCACHE_H
