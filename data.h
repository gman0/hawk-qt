#ifndef DATA_H
#define DATA_H

#include "thumbnailcache.h"
#include <hawk/Directory_cache.h>
#include <hawk/Stat_cache.h>

struct Data {
  hawk::Directory dir;
  hawk::Stat_cache::Data_ptr stats;
  Thumbnail_cache::Data_ptr thumbs;

  Data() {}

  Data(hawk::Directory&& d, hawk::Stat_cache::Data_ptr&& s,
       Thumbnail_cache::Data_ptr&& t)
      : dir{std::move(d)}, stats{std::move(s)}, thumbs{std::move(t)}
  {
  }
};

#endif // DATA_H
