#include "iocopy.h"
#include "utils.h"

IOCopy::IOCopy(const QList<QUrl>& srcs, const hawk::Path& dst, QObject* parent)
    : QObject(parent), hawk::IO_task_copy{to_paths_vector(srcs), dst}
{
}

void IOCopy::on_status_change(hawk::IO_task::Status st) noexcept
{
  emit status_changed(st);
}

void IOCopy::on_task_progress(const Total_progress& tp) noexcept
{
  emit task_progressed(tp);
}

void IOCopy::on_file_progress(const hawk::IO_task::File_progress& fp) noexcept
{
  emit file_progressed(fp);
}

void IOCopy::handle_error(const hawk::Filesystem_error& e,
                          hawk::IO_task::Target& t) noexcept
{
  emit error_occured(e, &t, this);
}

void IOCopy::handle_prepare_error(const hawk::Filesystem_error&) noexcept {}
