#-------------------------------------------------
#
# Project created by QtCreator 2017-02-06T20:49:58
#
#-------------------------------------------------

QT       += core gui quick
CONFIG += c++14 debug

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hawk-qt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

LIBS += -lhawk
INCLUDEPATH += /usr/local/include/hawk

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    apply.cpp \
    sort.cpp \
    utils.cpp \
    directorymodel.cpp \
    tasks.cpp \
    cursorcache.cpp \
    waitingspinnerwidget.cpp \
    columngroup.cpp \
    directorywidget.cpp \
    previewwidget.cpp \
    process.cpp \
    imagepreview.cpp \
    aspectratiopixmaplabel.cpp \
    elidedlabel.cpp \
    pathbar.cpp \
    pathbarbutton.cpp \
    directorysplit.cpp \
    filter.cpp \
    pathcompletionmodel.cpp \
    pathbaredit.cpp \
    commandbar.cpp \
    directoryitemdelegate.cpp \
    rootdirectorywidget.cpp \
    directorywidgetdata.cpp \
    iocontrol.cpp \
    iocopy.cpp \
    filterbar.cpp \
    focuslineedit.cpp \
    errorhandling.cpp \
    bulkrenamewindow.cpp \
    searchbar.cpp \
    advancedsearchresults.cpp \
    advancedsearch.cpp \
    advancedsearchwindow.cpp \
    ioexistswindow.cpp \
    iomove.cpp \
    fileoperations.cpp \
    removeconfirmwindow.cpp \
    ioremove.cpp \
#    annotatedscrollbar.cpp \
#    annotatedscrollbaroverlay.cpp
    newdirectorywindow.cpp \
    fileinfopreview.cpp \
    nopreview.cpp

HEADERS  += mainwindow.h \
    apply.h \
    sort.h \
    utils.h \
    directorymodel.h \
    data.h \
    cache.h \
    tasks.h \
    cursorcache.h \
    waitingspinnerwidget.h \
    columngroup.h \
    directorywidget.h \
    previewwidget.h \
    process.h \
    imagepreview.h \
    aspectratiopixmaplabel.h \
    elidedlabel.h \
    pathbar.h \
    pathbarbutton.h \
    directorysplit.h \
    filter.h \
    pathcompletionmodel.h \
    pathbaredit.h \
    commandbar.h \
    directoryitemdelegate.h \
    rootdirectorywidget.h \
    directorywidgetdata.h \
    iocontrol.h \
    iocopy.h \
    filterbar.h \
    focuslineedit.h \
    errorhandling.h \
    bulkrenamewindow.h \
    searchbar.h \
    advancedsearchresults.h \
    advancedsearch.h \
    advancedsearchwindow.h \
    thumbnailcache.h \
    ioexistswindow.h \
    iomove.h \
    fileoperation.h \
    fileoperations.h \
    removeconfirmwindow.h \
    ioremove.h \
    sortcriterium.h \
#    annotatedscrollbar.h \
#    annotatedscrollbaroverlay.h
    newdirectorywindow.h \
    fileinfopreview.h \
    nopreview.h \
    dirinfo.h

FORMS    += mainwindow.ui \
    iocontrol.ui \
    bulkrenamewindow.ui \
    advancedsearchwindow.ui \
    ioexistswindow.ui \
    removeconfirmwindow.ui \
    newdirectorywindow.ui \
    fileinfopreview.ui \
    nopreview.ui \
    fileoperations.ui \
    advancedsearchresultswindow.ui

DISTFILES += \
    textpreview.qml \
    execshell.qml \
    opentexteditor.qml

RESOURCES += \
    resources.qrc
