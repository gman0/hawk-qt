#ifndef PATHBAR_H
#define PATHBAR_H

#include "pathbarbutton.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QToolButton>

class PathBar : public QWidget {
private:
  Q_OBJECT

  QHBoxLayout* m_crumbs_layout;
  QWidget* m_crumbs;

  QToolButton* m_scrollStartBtn;
  QToolButton* m_scrollEndBtn;
  QScrollArea* m_scrollArea;

  hawk::Path m_path;

public:
  PathBar(QWidget* parent = nullptr);
  ~PathBar();

  const hawk::Path& path() const { return m_path; }

protected:
  void resizeEvent(QResizeEvent* event) override;
  void wheelEvent(QWheelEvent* event) override;
  void mouseReleaseEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;
  //  void enterEvent(QEvent* event) override;
  //  void leaveEvent(QEvent* event) override;

private:
  void add_crumb(const std::string& str);
  void add_separator();
  void clear();
  void update_scrollbar_visibility();
  void set_scrollbar_visibility();

public slots:
  void set_path(const hawk::Path& dirpath);

private slots:
  void crumb_click(const hawk::Path& dirpath);
  void scrollbtn_click();
  void setScrollButtonEnable(int value);

signals:
  void clicked(const hawk::Path& dirpath);
  void doubleClicked();
};

#endif // PATHBAR_H
