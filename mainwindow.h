#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dirinfo.h"
#include <QMainWindow>
#include <hawk/Path.h>
#include <vector>

class QLabel;
class FileOperations;
class DirectorySplit;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
private:
  Q_OBJECT

  Ui::MainWindow* ui;

  QTabWidget* m_tabs;
  FileOperations* m_ops;

  QLabel* m_tab_info;
  QMetaObject::Connection m_tab_info_conn;

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

public slots:
  int add_directory_tab(const hawk::Path& dirpath);
  int add_directory_tab_with_cursor(const hawk::Path& p);

private slots:
  void on_actionReload_triggered();

  void on_actionGo_back_triggered();

  void on_actionOpen_new_tab_triggered();

  void on_actionFile_operations_triggered();

  void on_actionSearch_triggered();

  void on_actionAdvanced_search_triggered();

  void on_actionChange_location_triggered();

  void on_actionFilter_triggered();

private:
  void init_directory_split(DirectorySplit* ds);
  void update_tab_names();
  DirectorySplit* current_directory_split();

  void set_tab_info(const DirInfo& i);
};

#endif // MAINWINDOW_H
