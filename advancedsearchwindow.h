#ifndef ADVANCEDSEARCHWINDOW_H
#define ADVANCEDSEARCHWINDOW_H

#include "advancedsearch.h"
#include <QMainWindow>
#include <hawk/Path.h>

namespace Ui {
class AdvancedSearchWindow;
}

class QCloseEvent;

class AdvancedSearchWindow : public QMainWindow {
private:
  Q_OBJECT

  Ui::AdvancedSearchWindow* ui;
  const hawk::Path m_base;

public:
  AdvancedSearchWindow(const hawk::Path& base, QWidget* parent = nullptr);
  ~AdvancedSearchWindow();

signals:
  void search_for(const hawk::Path& base, const AdvancedSearch::Criteria& cr);

protected:
  void closeEvent(QCloseEvent* event);

private:
  AdvancedSearch::Criteria generate_criteria() const;
};

#endif // ADVANCEDSEARCHWINDOW_H
