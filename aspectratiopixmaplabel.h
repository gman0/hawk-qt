#ifndef ASPECTRATIOPIXMAPLABEL_H
#define ASPECTRATIOPIXMAPLABEL_H

#include <QLabel>
#include <QPixmap>
#include <QResizeEvent>

class AspectRatioPixmapLabel : public QLabel {
private:
  Q_OBJECT

  QPixmap m_pix;

public:
  AspectRatioPixmapLabel(QWidget* parent = nullptr);

  int heightForWidth(int width) const override;
  QSize sizeHint() const override;
  QPixmap scaledPixmap() const;

public slots:
  void setPixmap(const QPixmap& p);
  void resizeEvent(QResizeEvent* e);
};

#endif // ASPECTRATIOPIXMAPLABEL_H
