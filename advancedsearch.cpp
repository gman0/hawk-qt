#include "advancedsearch.h"
#include <QDebug>
#include <QMessageBox>
#include <QProcess>

AdvancedSearch::AdvancedSearch(const hawk::Path& base, const Criteria& cr,
                               QObject* parent)
    : QObject{parent}, m_base{base}, m_cr{cr}, m_proc{nullptr}
{
}

AdvancedSearch::~AdvancedSearch() { cancel(); }

void AdvancedSearch::search()
{
  m_proc = new QProcess{this};
  m_proc->setProgram("find");
  m_proc->setArguments(parseCriteria());

  connect(m_proc, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(
                      &QProcess::finished),
          [this](int exit_code, QProcess::ExitStatus) {
            auto results = m_proc->readAllStandardOutput().split('\n');
            // Remove the last new line
            results.pop_back();

            if (exit_code != 0) {
              // FIXME has to run in GUI thread
              QMessageBox mbox;
              mbox.setIcon(QMessageBox::Critical);
              mbox.setStandardButtons(QMessageBox::Ok);
              mbox.setText(
                  QString("Following error(s) occurred while searching:\n%1")
                      .arg(QString(m_proc->readAllStandardError())));
              mbox.exec();
            }

            m_proc->deleteLater();
            m_proc = nullptr;

            emit searchFinished(results);
          });

  m_proc->start();
}

void AdvancedSearch::cancel()
{
  if (m_proc) {
    m_proc->kill();
    m_proc->deleteLater();
    m_proc = nullptr;
  }
}

QStringList AdvancedSearch::parseCriteria()
{
  QStringList args;

  args << m_base.c_str();

  on_criterion_set(Name,
                   [&](const QVariant& v) { args << "-name" << v.toString(); });

  on_criterion_set(Type,
                   [&](const QVariant& v) { args << "-type" << v.toString(); });

  on_criterion_set(MaxDepth, [&](const QVariant& v) {
    args << "-maxdepth" << QString::number(v.toInt());
  });

  on_criterion_set(RegExp, [&](const QVariant& v) {
    args << "-regextype"
         << "posix-extended";
    args << "-regex" << v.toString();
  });

  return args;
}
