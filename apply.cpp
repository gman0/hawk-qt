#include "apply.h"
#include <hawk/Filesystem.h>

using namespace hawk;

void apply_stat(const Path& base, Stat_cache::Data_raw_ptr sc,
                Directory::Static st, Static_idx idx)
{
  sc->map[idx.value()] = symlink_status(base / st.name(idx.value()));
}

void apply_thumbnail(const Path& base,
                     std::vector<std::pair<uint64_t, QImage>>& thumbs,
                     Directory::Static st, Static_idx idx)
{
  QImage pix(QString((base / st.name(idx)).c_str()));
  if (pix.isNull())
    return;

  pix = pix.scaled(16, 16);
  thumbs.push_back({idx.value(), std::move(pix)});
}
