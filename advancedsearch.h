#ifndef ADVANCEDSEARCH_H
#define ADVANCEDSEARCH_H

#include <QObject>
#include <QVariant>
#include <hawk/Path.h>
#include <map>

class QProcess;

class AdvancedSearch : public QObject {

private:
  Q_OBJECT

public:
  enum Criterion { Name, RegExp, Type, MaxDepth };
  using Criteria = std::map<Criterion, QVariant>;

private:
  const hawk::Path m_base;
  Criteria m_cr;
  QProcess* m_proc;

public:
  AdvancedSearch(const hawk::Path& base, const Criteria& cr,
                 QObject* parent = nullptr);
  ~AdvancedSearch();

public slots:
  void search();
  void cancel();

signals:
  void searchFinished(const QList<QByteArray>& results);

private:
  QStringList parseCriteria();

  template <typename Callable> void on_criterion_set(Criterion c, Callable&& f)
  {
    auto found = m_cr.find(c);
    if (found != m_cr.end())
      f(found->second);
  }
};

#endif // ADVANCEDSEARCH_H
