#include "iocontrol.h"
#include "ui_iocontrol.h"
#include "utils.h"
#include <QDebug>

namespace {

QString fileoperation_to_text(FileOperation type)
{
  switch (type) {
  case FileOperation::cp:
    return "Copy";
  case FileOperation::ln:
    return "Link";
  case FileOperation::mv:
    return "Move";
  case FileOperation::rm:
    return "Delete";
  }

  return "N/A";
}

} // unnamed-namespace

IOControl::IOControl(FileOperation type, QWidget* parent)
    : QWidget(parent), ui(new Ui::IOControl), m_type{type}
{
  ui->setupUi(this);
  ui->operation_label->setText(fileoperation_to_text(type));

  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

  connect(ui->contButton, &QPushButton::clicked,
          [this] { emit cont_clicked(); });
  connect(ui->cancelButton, &QPushButton::clicked,
          [this] { emit cancel_clicked(); });

  if (type == FileOperation::rm) {
    ui->contButton->hide();
    ui->total_progress->hide();
    ui->file_progress->hide();
  }
}

IOControl::~IOControl() { delete ui; }

void IOControl::set_task_info(const hawk::IO_task::Total_progress& tp)
{
  ui->src_label->setText(tp.src.c_str());
  ui->dst_label->setText(tp.dst.c_str());
  ui->total_progress->setValue((double)tp.offset / tp.size * 100);
}

void IOControl::set_status_info(hawk::IO_task::Status st)
{
  QString desc;

  using St = hawk::IO_task::Status;

  switch (st) {
  case St::not_started:
    desc = "Not started";
    break;
  case St::preparing:
    desc = "Preparing";
    break;
  case St::pending:
    desc = "Pending";
    break;
  case St::finished:
    desc = "Finished";
    break;
  case St::failed:
    desc = "Failed";
    break;
  case St::paused:
    desc = "Paused";
    break;
  case St::canceled:
    desc = "Canceled";
    break;
  }

  switch (st) {
  case St::preparing:
  case St::finished:
    ui->file_progress->setValue(100);
    ui->total_progress->setValue(100);
    ui->rate_label->setText("N/A");
    ui->eta_label->setText("N/A");
  case St::failed:
  case St::canceled:
    ui->contButton->setEnabled(false);
    ui->cancelButton->setEnabled(false);
  default:
    ui->contButton->setEnabled(true);
    ui->cancelButton->setEnabled(true);
  }

  if (st == St::pending)
    ui->contButton->setText("Pause");
  else if (st == St::paused)
    ui->contButton->setText("Resume");

  ui->status_label->setText(desc);
}

void IOControl::set_file_info(const hawk::IO_task::File_progress& fp)
{
  char buf[100];
  ui->rate_label->setText(
      QString("%1/s").arg(human_readable_filesize(fp.rate, buf)));
  ui->eta_label->setText(human_readable_time(fp.eta, buf));

  ui->file_progress->setValue((double)fp.offset / fp.size * 100);
}
