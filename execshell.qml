import QtQuick 2.0
import hawk.process 1.0

Item {
    Process {
        id: proc
    }

    function execShell(cmd) {
        proc.start('sh', [cmd])
        proc.waitForFinished()

        return proc.exitCode()
    }
}
