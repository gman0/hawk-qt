#ifndef SEARCHBAR_H
#define SEARCHBAR_H

#include "advancedsearch.h"
#include <QWidget>
#include <hawk/Path.h>

class ColumnGroup;

class SearchBar : public QWidget {
private:
  Q_OBJECT

  ColumnGroup* m_cg;

public:
  SearchBar(ColumnGroup* cg, QWidget* parent = 0);

public slots:
  void show_advanced_search();

signals:
  void searchChanged(const QString& term);
  void focusLost();
  void advancedSearch(const hawk::Path& base,
                      const AdvancedSearch::Criteria& cr);
};

#endif // SEARCHBAR_H
