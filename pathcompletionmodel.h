#ifndef PATHCOMPLETIONMODEL_H
#define PATHCOMPLETIONMODEL_H

#include <QAbstractItemModel>
#include <hawk/Directory.h>
#include <hawk/Tasking.h>

class Update_completion_model_task : public QObject,
                                     public hawk::Tasking::Committing_task {
  Q_OBJECT

public:
  Update_completion_model_task() : QObject{}, Committing_task{0} {}

  void commit() noexcept override { emit dataReady(); }

signals:
  void dataReady();
};

class PathCompletionModel : public QAbstractItemModel {
private:
  Q_OBJECT

  hawk::Tasking m_ts;
  hawk::Directory m_dir;

public:
  PathCompletionModel(QObject* parent = nullptr);

  QVariant data(const QModelIndex& index, int role) const override;
  QModelIndex index(int row, int column,
                    const QModelIndex& parent) const override;
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent) const override;
  QModelIndex parent(const QModelIndex& child) const override;

public slots:
  void set_path(const hawk::Path& dirpath);
};

#endif // PATHCOMPLETIONMODEL_H
