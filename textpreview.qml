import QtQuick 2.5
import hawk.process 1.0

Item {
    Process {
        id: proc
    }

    function getFileContents(filepath) {
        proc.start('highlight', [ '-i', filepath, '-I', /*'-l',*/ '-t', 4, '-u', 'utf8',  '--force', '-q' ])
        var contents = proc.readTillEOF()
        proc.waitForFinished()

        return contents
    }
}
