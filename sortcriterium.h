#ifndef SORTCRITERIUM_H
#define SORTCRITERIUM_H

enum class SortCriterium {
  natural = 1,
  natural_rev = -1,
  size = 2,
  size_rev = -2
};

#endif // SORTCRITERIUM_H
