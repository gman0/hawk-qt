#include "cursorcache.h"
#include <hawk/Filesystem.h>

CursorCache::CursorCache(const CursorCache& cursors) : QObject(cursors.parent())
{
  std::lock_guard<std::mutex> lk{cursors.m_mtx};
  m_cursors = cursors.m_cursors;
}

CursorCache& CursorCache::operator=(const CursorCache& cursors)
{
  if (this != &cursors) {
    std::lock_guard<std::mutex> lk{cursors.m_mtx};
    m_cursors = cursors.m_cursors;
  }

  return *this;
}

std::pair<Cursor, bool> CursorCache::find(const hawk::Path& dirpath)
{
  std::lock_guard<std::mutex> lk{m_mtx};

  auto found = m_cursors.find(dirpath);
  if (found != m_cursors.end())
    return {found->second, true};

  return {Cursor(), false};
}

void CursorCache::store(const hawk::Path& dirpath, const Cursor& c)
{
  if (!dirpath.empty()) {
    std::lock_guard<std::mutex> lk{m_mtx};
    m_cursors[dirpath] = c;
  }
}

void CursorCache::store(const hawk::Path& path)
{
  for (const hawk::Path& p : path) {
    int err;
    auto st = hawk::status(p, err);
    if (err != 0)
      return;

    store(p.parent_path(), Cursor{0, st.st_ino, p.filename()});
  }
}
