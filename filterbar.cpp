#include "filterbar.h"
#include "focuslineedit.h"
#include <QCheckBox>
#include <QHBoxLayout>

FilterBar::FilterBar(QWidget* parent) : QWidget{parent}
{
  QHBoxLayout* layout = new QHBoxLayout();
  layout->setContentsMargins(0, 0, 0, 0);

  FocusLineEdit* edit = new FocusLineEdit(this);
  edit->setClearButtonEnabled(true);
  edit->setPlaceholderText("Filter by name (text between / and / is "
                           "interpreted as a regular expression)");

  layout->addWidget(edit);

  setLayout(layout);

  connect(edit, &FocusLineEdit::returnPressed,
          [edit, this] { emit filterChanged(edit->text()); });
  connect(edit, &FocusLineEdit::focusLost, [this] { emit focusLost(); });
}
