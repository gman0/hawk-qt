#include "aspectratiopixmaplabel.h"

AspectRatioPixmapLabel::AspectRatioPixmapLabel(QWidget* parent) : QLabel{parent}
{
  setMinimumSize(1, 1);
  setScaledContents(false);
}

int AspectRatioPixmapLabel::heightForWidth(int width) const
{
  if (m_pix.isNull())
    return height();

  return static_cast<qreal>(m_pix.height() * width) / m_pix.width();
}

QSize AspectRatioPixmapLabel::sizeHint() const
{
  return QSize{width(), heightForWidth(width())};
}

QPixmap AspectRatioPixmapLabel::scaledPixmap() const
{
  return m_pix.scaled(size(), Qt::KeepAspectRatio);
}

void AspectRatioPixmapLabel::setPixmap(const QPixmap& p)
{
  m_pix = p;
  QLabel::setPixmap(scaledPixmap());
}

void AspectRatioPixmapLabel::resizeEvent(QResizeEvent*)
{
  if (!m_pix.isNull())
    QLabel::setPixmap(scaledPixmap());
}
