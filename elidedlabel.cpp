#include "elidedlabel.h"
#include <QDebug>
#include <QResizeEvent>

ElidedLabel::ElidedLabel(QWidget* parent, Qt::WindowFlags f)
    : QLabel{parent, f}, m_mode{Qt::ElideRight}
{
}

ElidedLabel::ElidedLabel(const QString& text, QWidget* parent,
                         Qt::WindowFlags f)
    : QLabel{parent, f}, m_mode{Qt::ElideRight}, m_text{text}
{
  updateElidedText();
}

Qt::TextElideMode ElidedLabel::elideMode() const { return m_mode; }

void ElidedLabel::setElideMode(Qt::TextElideMode mode)
{
  if (m_mode != mode) {
    m_mode = mode;
    updateElidedText();
    updateGeometry();
    update();
  }
}

QSize ElidedLabel::minimumSizeHint() const
{
  if (m_mode == Qt::ElideNone)
    return sizeHint();

  QFontMetrics fm = fontMetrics();
  return QSize(fm.width("..."), fm.height());
}

QString ElidedLabel::text() const { return m_text; }

void ElidedLabel::setText(const QString& text)
{
  m_text = text;
  updateElidedText();
}
/*
void ElidedLabel::resizeEvent(QResizeEvent* event)
{
  updateElidedText(event->size().width());
  QLabel::resizeEvent(event);
}
*/

void ElidedLabel::updateElidedText(int width)
{
  QString elided = fontMetrics().elidedText(m_text, m_mode, width);
  qDebug() << fontMetrics().width(elided) << width;
  QLabel::setText(elided);
}

void ElidedLabel::updateElidedText()
{
  updateElidedText(contentsRect().width());
}
