#ifndef DIRECTORYSPLIT_H
#define DIRECTORYSPLIT_H

#include "advancedsearch.h"
#include "cursorcache.h"
#include "fileoperation.h"
#include <QList>
#include <QUrl>
#include <QWidget>
#include <hawk/Path.h>

class ColumnGroup;
class CommandBar;

class DirectorySplit : public QWidget {
  Q_OBJECT

  ColumnGroup* m_cg;
  CommandBar* m_cmdbar;

public:
  DirectorySplit(QWidget* parent = nullptr);
  // `dirpath' is used for Cursor `c'
  DirectorySplit(const hawk::Path& dirpath, const Cursor& c,
                 QWidget* parent = nullptr);

  void set_cursors(const CursorCache& cursors);

  hawk::Path dirpath();
  void reload_path();
  void set_path(const hawk::Path& dirpath);

  ColumnGroup* columngroup() const;

public slots:
  void show_pathbar_edit();
  void show_searchbar();
  void show_filterbar();
  void show_advanced_search();

signals:
  void path_changed(const hawk::Path& dirpath);

  void new_tab_request(const hawk::Path& dirpath);

  void advanced_search(const hawk::Path& base,
                       const AdvancedSearch::Criteria& cr);

  void io_request(FileOperation op, const QList<QUrl>& srcs,
                  const hawk::Path& dst);

private:
  void init();
  void init_cg();
  void init_cmdbar();
};

#endif // DIRECTORYSPLIT_H
