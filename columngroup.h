#ifndef COLUMNGROUP_H
#define COLUMNGROUP_H

#include "cache.h"
#include "cursorcache.h"
#include "directorywidget.h"
#include "fileoperation.h"
#include "previewwidget.h"
#include "rootdirectorywidget.h"
#include <QHBoxLayout>
#include <QList>
#include <QSplitter>
#include <QUrl>
#include <QWidget>
#include <hawk/watchdog/Watchdog.h>
#include <memory>
#include <mutex>
#include <vector>

class ColumnGroup : public QSplitter {
private:
  Q_OBJECT

  hawk::Path m_dirpath;
  unsigned m_page;

  Cache m_cache;
  CursorCache m_cursors;
  hawk::Watchdog m_wd;
  std::recursive_mutex m_mtx;

  std::vector<DirectoryWidget*> m_parents;
  RootDirectoryWidget* m_root;
  PreviewWidget* m_preview;

  hawk::Directory_data::Filters m_active_filters;
  size_t m_filters_id;

public:
  ColumnGroup(int columns, QWidget* parent = nullptr);

  hawk::Path dirpath();
  CursorCache& cursors() { return m_cursors; }
  const CursorCache& cursors() const { return m_cursors; }

  void set_cursors(const CursorCache& cursors) { m_cursors = cursors; }

public slots:
  void set_path(hawk::Path dirpath, unsigned page);
  void reload_path();

  void filter_by_name(const QString& str);

  DirInfo dirinfo() const;

private slots:
  void side_click(const hawk::Path& p);

signals:
  void path_changed(const hawk::Path& dirpath);
  void new_tab_request(const hawk::Path& dirpath);

  void io_request(FileOperation op, const QList<QUrl>& srcs,
                  const hawk::Path& dst);

  void error_occured(hawk::Filesystem_error e);

  void update_dirinfo(DirInfo i);

private:
  void on_wd_event(hawk::Monitor::Event e, const hawk::Path& dirpath) noexcept;
  void release();

  void update_filters(hawk::Directory_data::Filters&& fs);
};

#endif // COLUMNGROUP_H
