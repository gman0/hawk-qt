#include "errorhandling.h"
#include <QApplication>
#include <QMessageBox>
#include <iostream>

using namespace std;

void display_filesystem_error(const hawk::Filesystem_error& e)
{
  std::cerr << "Filesystem error: " << e.what() << std::endl;

  QWidget* win = QApplication::activeWindow();
  if (!win)
    return;

  QMessageBox mbox(win);
  mbox.setIcon(QMessageBox::Critical);
  mbox.setStandardButtons(QMessageBox::Ok);
  mbox.setDefaultButton(QMessageBox::Ok);
  mbox.setEscapeButton(QMessageBox::Ok);
  mbox.setText(QString("Filesystem error: %1").arg(e.what()));
  mbox.show();

  QPoint p = win->pos();
  mbox.move(p.x() + win->width() / 2 - mbox.width() / 2,
            p.y() + win->height() / 2);
  mbox.exec();
}
