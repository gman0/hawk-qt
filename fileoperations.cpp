#include "fileoperations.h"
#include "iocopy.h"
#include "ioexistswindow.h"
#include "iomove.h"
#include "ioremove.h"
#include "ui_fileoperations.h"

using namespace std;

FileOperations::FileOperations(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::FileOperations)
{
  ui->setupUi(this);

  ui->contentsLayout->setAlignment(Qt::AlignTop);

  connect(ui->clearBtn, &QPushButton::clicked, this,
          &FileOperations::clear_finished);
}

FileOperations::~FileOperations() { delete ui; }

namespace {

template <typename Operation>
IOTask create_task(const QList<QUrl>& srcs, const hawk::Path& dst,
                   FileOperations* parent, FileOperation type)
{
  IOTask t = {make_unique<Operation>(srcs, dst),
              make_unique<IOControl>(type, parent)};

  Operation* op = static_cast<Operation*>(t.first.get());
  IOControl* ctl = static_cast<IOControl*>(t.second.get());

  QObject::connect(op, &Operation::status_changed, ctl,
                   &IOControl::set_status_info);
  QObject::connect(op, &Operation::task_progressed, ctl,
                   &IOControl::set_task_info);
  QObject::connect(op, &Operation::file_progressed, ctl,
                   &IOControl::set_file_info);
  QObject::connect(op, &Operation::error_occured, parent,
                   &FileOperations::handler_error);
  QObject::connect(ctl, &IOControl::cont_clicked, [op] {
    using St = hawk::IO_task::Status;
    St s = op->get_status();
    if (s == St::pending)
      op->pause();
    else if (s == St::paused)
      op->start();
  });
  QObject::connect(ctl, &IOControl::cancel_clicked, [op] { op->cancel(); });

  return t;
}

} // unnamed-namespace

void FileOperations::add(FileOperation type, const QList<QUrl>& srcs,
                         const hawk::Path& dst)
{
  IOTask t;
  switch (type) {
  case FileOperation::cp:
    t = create_task<IOCopy>(srcs, dst, this, type);
    break;
  case FileOperation::mv:
    t = create_task<IOMove>(srcs, dst, this, type);
    break;
  case FileOperation::rm:
    t = create_task<IORemove>(srcs, dst, this, type);
  default:
    break;
  }

  ui->contentsLayout->insertWidget(0, t.second.get());

  t.first->start();

  m_tasks.push_back(std::move(t));
  show();
}

void FileOperations::handle_exists_error(hawk::IO_task::Target& t)
{
  IOExistsWindow* w = new IOExistsWindow(t.src.c_str(), t.dst.c_str(), this);
  connect(w, &IOExistsWindow::cancel, [&] { t.src.clear(); });
  connect(w, &IOExistsWindow::overwrite, [&] {
    int err;
    hawk::remove_file(t.dst, err);
  });
  connect(w, &IOExistsWindow::rename, [&] { t.dst = t.dst.string() + "_"; });
  w->show();
}

void FileOperations::handler_error(hawk::Filesystem_error e,
                                   hawk::IO_task::Target* t,
                                   hawk::IO_task* parent)
{
  if (e.get_errno() == EEXIST) {
    IOExistsWindow* w =
        new IOExistsWindow(t->src.c_str(), t->dst.c_str(), this);

    connect(w, &IOExistsWindow::overwrite, [t, parent] {
      int err;
      hawk::remove_file(t->dst, err);
      parent->start();
    });

    connect(w, &IOExistsWindow::rename, [t, parent] {
      t->dst = t->dst.string() + "_";
      parent->start();
    });

    w->show();
  }
}

void FileOperations::clear_finished()
{
  m_tasks.remove_if([](const IOTask& t) {
    return t.first->get_status() == hawk::IO_task::Status::finished;
  });
}
