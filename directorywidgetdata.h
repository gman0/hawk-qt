#ifndef DIRECTORYWIDGETDATA_H
#define DIRECTORYWIDGETDATA_H

#include "cache.h"
#include "cursorcache.h"
#include "directorymodel.h"
#include "tasks.h"
#include "waitingspinnerwidget.h"
#include <QAbstractItemView>
#include <QItemSelectionModel>
#include <chrono>
#include <hawk/Path.h>
#include <hawk/Tasking.h>
#include <hawk/watchdog/Watchdog.h>
#include <iostream>
#include <memory>
#include <mutex>

struct DirectoryWidgetData {
  mutable std::mutex m;
  hawk::Path dirpath;
  unsigned page;

  hawk::Tasking* ts;
  hawk::Watchdog& wd;
  Cache& cache;
  CursorCache& cursors;

  hawk::Directory_data::Filters filters;
  size_t filters_id;

  DirectoryModel model;
  QItemSelectionModel selmodel;
  WaitingSpinnerWidget wspinner;

  DirectoryWidgetData(hawk::Watchdog& w, Cache& c, CursorCache& cc,
                      hawk::Tasking* tasking,
                      hawk::Tasking::Exception_handler eh,
                      QAbstractItemView* parent);
  ~DirectoryWidgetData();
  DirectoryWidgetData(const DirectoryWidgetData&) = delete;
  DirectoryWidgetData& operator=(const DirectoryWidgetData&) = delete;

  hawk::Path get_dirpath() const;

  void release();
  void set_filters(const hawk::Directory_data::Filters& fs, size_t id);

private:
  bool _owns_tasking;
};

#endif // DIRECTORYWIDGETDATA_H
