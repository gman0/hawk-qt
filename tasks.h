#ifndef TASKS_H
#define TASKS_H

#include "cache.h"
#include "cursorcache.h"
#include "data.h"
#include "directorymodel.h"
#include "thumbnailcache.h"
#include <QObject>
#include <QQmlEngine>
#include <functional>
#include <hawk/Path.h>
#include <hawk/Tasking.h>
#include <hawk/watchdog/Watchdog.h>

#include <iostream>

enum class TaskPriorty {
  setPath_load_dcache = 4,
  reloadPath_load_dcache = 3,

  sort = 2,
  load_scache = 1,
  set_cursor = 1,

  discard = 0,

  preview = 4,

  interrupt = 5
};

using GetRegion = std::function<hawk::Region()>;

class QTreeView;

class Scache_region_scrolling {
  QTreeView* m_view;
};

class Load_dcache_task : public hawk::Tasking::Task {
  Cache& m_cache;
  hawk::Watchdog& m_wd;

  const hawk::Path m_dirpath;
  const unsigned m_page;

  hawk::Directory_cache::Data_ptr m_commit_data;
  hawk::Directory_data::Filters m_filters;

public:
  Load_dcache_task(TaskPriorty p, Cache& c, hawk::Watchdog& wd,
                   hawk::Directory_data::Filters filters,
                   const hawk::Path& dirpath, unsigned page)
      : hawk::Tasking::Task{static_cast<unsigned>(p)}, m_cache{c}, m_wd{wd},
        m_dirpath{dirpath}, m_page{page}, m_filters{std::move(filters)}
  {
  }

  void run() override;
  void commit() noexcept override;
};

class Discard_dcache_task : public hawk::Tasking::Committing_task {
  Cache& m_cache;
  const hawk::Path m_dirpath;

public:
  Discard_dcache_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath)
      : hawk::Tasking::Committing_task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}
  {
  }

  void commit() noexcept override;
};

class Load_scache_task : public hawk::Tasking::Task {
  Cache& m_cache;
  const hawk::Path m_dirpath;
  const unsigned m_page;

  GetRegion getregion;

  hawk::Stat_cache::Data_ptr m_commit_data;

public:
  Load_scache_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath,
                   unsigned page, GetRegion f)
      : hawk::Tasking::Task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}, m_page{page}, getregion{std::move(f)}
  {
  }

  void run() override;
  void commit() noexcept override;
};

class Discard_scache_task : public hawk::Tasking::Committing_task {
  Cache& m_cache;

  const hawk::Path m_dirpath;

public:
  Discard_scache_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath)
      : hawk::Tasking::Committing_task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}
  {
  }

  void commit() noexcept override;
};

template <typename Compare> class Sort_task : public hawk::Tasking::Task {
  Cache& m_cache;
  const hawk::Path m_dirpath;
  const unsigned m_page;

public:
  Sort_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath, unsigned page)
      : hawk::Tasking::Task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}, m_page{page}
  {
  }

  void run()
  {
    std::cout << "# Sort_task:run()" << std::endl;
    auto data = m_cache.dcache.find(m_dirpath, m_page);
    hawk::Directory dir(m_dirpath, m_page, data);
    dir.sort(Compare(m_dirpath, dir.static_access()));
  }

  void commit() noexcept { std::cout << "# Sort_task:commit()" << std::endl; }
};

// Creates Sort_task which honours current QSettings sort criterium
std::unique_ptr<hawk::Tasking::Task> create_sort_task(TaskPriorty p, Cache& c,
                                                      const hawk::Path& dirpath,
                                                      unsigned page);

class Update_dirmodel_task : public QObject,
                             public hawk::Tasking::Committing_task {
  Q_OBJECT

  Cache& m_cache;
  DirectoryModel& m_model;
  const hawk::Path m_dirpath;
  const unsigned m_page;

public:
  Update_dirmodel_task(TaskPriorty p, Cache& c, DirectoryModel& m,
                       const hawk::Path& dirpath, unsigned page)
      : Committing_task{static_cast<unsigned>(p)}, m_cache{c}, m_model{m},
        m_dirpath{dirpath}, m_page{page}
  {
  }

  void commit() noexcept override;

signals:
  void modelUpdated(Data data);
  void viewReady();
};

class Scanned_dirmodel_task : public QObject,
                              public hawk::Tasking::Committing_task {
  Q_OBJECT

  const int m_from;
  const int m_to;

public:
  Scanned_dirmodel_task(TaskPriorty p, int from, int to)
      : Committing_task{static_cast<unsigned>(p)}, m_from{from}, m_to{to}
  {
  }

  void commit() noexcept override;

signals:
  void scannedItems(int from, int to);
};

class Set_cursor_task : public QObject, public hawk::Tasking::Task {
  Q_OBJECT

  Cache& m_cache;
  CursorCache& m_cursors;
  const hawk::Path m_dirpath;
  const unsigned m_page;

  int m_row;
  Cursor m_cursor;

public:
  Set_cursor_task(TaskPriorty p, Cache& c, CursorCache& cursors,
                  const hawk::Path& dirpath, unsigned page)
      : Task{static_cast<unsigned>(p)}, m_cache{c}, m_cursors{cursors},
        m_dirpath{dirpath}, m_page{page}, m_row{0}
  {
  }

  void run() override;
  void commit() noexcept override;

signals:
  void cursorSet(int row);
};

class Unload_scache_task : public hawk::Tasking::Committing_task {
  hawk::Directory& m_dir;
  hawk::Stat_cache::Data_ptr& m_scache;
  const int m_at;

public:
  Unload_scache_task(TaskPriorty p, int at, hawk::Directory& dir,
                     hawk::Stat_cache::Data_ptr& scache)
      : Committing_task{static_cast<unsigned>(p)}, m_dir{dir}, m_scache{scache},
        m_at{at}
  {
  }

  void commit() noexcept override;
};

class Load_tcache_task : public hawk::Tasking::Task {
  Cache& m_cache;
  const hawk::Path m_dirpath;
  const unsigned m_page;

  GetRegion getregion;
  hawk::Region_list m_rs;

  std::vector<std::pair<uint64_t, QImage>> m_thumbs;

public:
  Load_tcache_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath,
                   unsigned page, GetRegion f)
      : hawk::Tasking::Task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}, m_page{page}, getregion{std::move(f)}
  {
  }

  void run() override;
  void commit() noexcept override;
};

class Discard_tcache_task : public hawk::Tasking::Committing_task {
  Cache& m_cache;

  const hawk::Path m_dirpath;

public:
  Discard_tcache_task(TaskPriorty p, Cache& c, const hawk::Path& dirpath)
      : hawk::Tasking::Committing_task{static_cast<unsigned>(p)}, m_cache{c},
        m_dirpath{dirpath}
  {
  }

  void commit() noexcept override;
};

class Unload_tcache_task : public hawk::Tasking::Committing_task {
  hawk::Directory& m_dir;
  Thumbnail_cache::Data_ptr& m_tcache;
  const int m_at;

public:
  Unload_tcache_task(TaskPriorty p, int at, hawk::Directory& dir,
                     Thumbnail_cache::Data_ptr& tcache)
      : Committing_task{static_cast<unsigned>(p)}, m_dir{dir}, m_tcache{tcache},
        m_at{at}
  {
  }

  void commit() noexcept override;
};

class Text_preview_task : public QObject, public hawk::Tasking::Task {
  Q_OBJECT

  const hawk::Path m_path;
  QQmlEngine m_engine;
  QString m_contents;

public:
  Text_preview_task(TaskPriorty p, const hawk::Path& path)
      : Task{static_cast<unsigned>(p)}, m_path{path}
  {
  }

  void run() override;
  void commit() noexcept override;

signals:
  void contentsReady(QString html);
};

class Load_image_task : public QObject, public hawk::Tasking::Task {
  Q_OBJECT

  const hawk::Path m_path;
  std::unique_ptr<QImage> m_image;

public:
  Load_image_task(TaskPriorty p, const hawk::Path& path)
      : Task{static_cast<unsigned>(p)}, m_path{path},
        m_image{std::make_unique<QImage>()}
  {
  }

  void run() override;
  void commit() noexcept override;

signals:
  void imageReady(QImage* img);
};

class Empty_task : public hawk::Tasking::Committing_task {
public:
  Empty_task(TaskPriorty p) : Committing_task{static_cast<unsigned>(p)} {}
  void commit() noexcept {}
};

#endif // TASKS_H
