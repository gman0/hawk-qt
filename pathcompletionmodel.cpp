#include "pathcompletionmodel.h"
#include "filter.h"
#include "sort.h"
#include <chrono>
#include <exception>
#include <iostream>
#include <memory>

using namespace std;

namespace {

void handle_exception(exception_ptr eptr) noexcept
{
  try {
    throw eptr;
  }
  catch (const hawk::Filesystem_error& e) {
    cerr << "Filesystem error: " << e.what() << endl;
  }
}

enum class Completion_priorities { load = 2, sort = 1, update = 0 };

class Load_data_task : public hawk::Tasking::Task {
  hawk::Directory& m_dir;
  unique_ptr<hawk::Directory_data> m_commiting_data;

  const hawk::Path m_dirpath;

public:
  Load_data_task(hawk::Directory& dir, const hawk::Path& dirpath)
      : Task{static_cast<unsigned>(Completion_priorities::load)}, m_dir{dir},
        m_commiting_data{make_unique<hawk::Directory_data>(1000000)},
        m_dirpath{dirpath}
  {
  }

  void run() override
  {
    m_commiting_data->set_path(m_dirpath, 0, {Filter_ndirectories()});
  }

  void commit() noexcept override
  {
    m_dir = hawk::Directory(m_dirpath, 0, std::move(m_commiting_data));
  }
};

class Sort_data_task : public hawk::Tasking::Task {
  hawk::Directory& m_dir;

public:
  Sort_data_task(hawk::Directory& dir)
      : Task{static_cast<unsigned>(Completion_priorities::sort)}, m_dir{dir}
  {
  }

  void run() override
  {
    m_dir.sort(Sort_natural(hawk::Path(), m_dir.static_access()));
  }
  void commit() noexcept override {}
};

} // unnamed-namespace

PathCompletionModel::PathCompletionModel(QObject* parent)
    : QAbstractItemModel{parent}, m_ts{chrono::milliseconds{500},
                                       [this](exception_ptr eptr) {
                                         handle_exception(eptr);
                                         m_dir.release();
                                       }}
{
}

QVariant PathCompletionModel::data(const QModelIndex& index, int role) const
{
  if (role == Qt::DisplayRole || role == Qt::EditRole)
    return m_dir.sorted_access().name(index.row());

  return QVariant();
}

QModelIndex PathCompletionModel::index(int row, int, const QModelIndex&) const
{
  return createIndex(row, 0);
}

int PathCompletionModel::rowCount(const QModelIndex&) const
{
  if (m_dir.valid())
    return m_dir.size();

  return 0;
}

int PathCompletionModel::columnCount(const QModelIndex&) const { return 1; }

QModelIndex PathCompletionModel::parent(const QModelIndex&) const
{
  return QModelIndex();
}

void PathCompletionModel::set_path(const hawk::Path& dirpath)
{
  beginResetModel();
  m_dir.release();
  endResetModel();

  auto load_data = make_unique<Load_data_task>(m_dir, dirpath);
  auto sort_dir = make_unique<Sort_data_task>(m_dir);
  auto update_model = make_unique<Update_completion_model_task>();

  connect(update_model.get(), &Update_completion_model_task::dataReady, [this] {
    beginResetModel();
    endResetModel();
  });

  m_ts.run(std::move(load_data), std::move(sort_dir), std::move(update_model));
}
