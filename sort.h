#ifndef SORT_H
#define SORT_H

#include <QCollator>
#include <hawk/Directory.h>
#include <vector>

struct Sort_alpha {
  hawk::Directory::Static st;
  Sort_alpha(const hawk::Path&, hawk::Directory::Static static_access)
      : st{static_access}
  {
  }
  bool operator()(hawk::Static_idx a, hawk::Static_idx b);
};

struct Sort_natural {
  hawk::Directory::Static st;
  QCollator c;
  std::vector<QCollatorSortKey> keys;

  Sort_natural(const hawk::Path&, hawk::Directory::Static static_access);
  bool operator()(hawk::Static_idx a, hawk::Static_idx b);
};

struct Sort_natural_reverse {
  hawk::Directory::Static st;
  QCollator c;
  std::vector<QCollatorSortKey> keys;

  Sort_natural_reverse(const hawk::Path&,
                       hawk::Directory::Static static_access);
  bool operator()(hawk::Static_idx a, hawk::Static_idx b);
};

struct Sort_size {
  hawk::Directory::Static st;
  std::vector<uintmax_t> sizes;

  Sort_size(const hawk::Path& dirpath, hawk::Directory::Static static_access);
  bool operator()(hawk::Static_idx a, hawk::Static_idx b);
};

struct Sort_size_reverse {
  hawk::Directory::Static st;
  std::vector<uintmax_t> sizes;

  Sort_size_reverse(const hawk::Path& dirpath,
                    hawk::Directory::Static static_access);
  bool operator()(hawk::Static_idx a, hawk::Static_idx b);
};

#endif // SORT_H
