#ifndef PROCESS_H
#define PROCESS_H

#include <QProcess>
#include <QVariant>

class Process : public QProcess {
  Q_OBJECT

public:
  Process(QObject* parent = nullptr) : QProcess{parent} {}

  Q_INVOKABLE void start(const QString& program, const QVariantList& args);
  Q_INVOKABLE void waitForFinished();
  Q_INVOKABLE QByteArray readAll();
  Q_INVOKABLE QByteArray readAllBlocking();
  Q_INVOKABLE QByteArray readTillEOF();
  Q_INVOKABLE int exitCode() const;
};

#endif // PROCESS_H
