#include "imagepreview.h"
#include <QDebug>
#include <QResizeEvent>
#include <QSpacerItem>

ImagePreview::ImagePreview(QWidget* parent) : QWidget{parent}
{
  m_imglabel = new AspectRatioPixmapLabel(this);
  m_imglabel->setMinimumSize(1, 1);
  setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

  setLayout(&m_vbox);
  m_vbox.addStretch(1);
  m_vbox.addWidget(m_imglabel);
  m_vbox.addWidget(&m_res);
  m_vbox.addStretch(1);
}

void ImagePreview::set_image(QImage* img)
{
  m_img.reset(img);

  m_imglabel->setPixmap(QPixmap::fromImage(*img));

  m_res.setText(
      QString("%1x%2 px")
          .arg(QString::number(img->width()), QString::number(img->height())));
}
