#include "directorywidget.h"
#include "directoryitemdelegate.h"
#include "filter.h"
#include "sort.h"
#include <QDebug>
#include <QDropEvent>
#include <QMenu>
#include <QMimeData>
#include <QScrollBar>

using namespace std;

DirectoryWidget::DirectoryWidget(hawk::Watchdog& w, Cache& c, CursorCache& cc,
                                 hawk::Tasking* ts, QWidget* parent)
    : QListView{parent}
{
  hawk::Tasking::Exception_handler eh;
  if (!ts) {
    eh = [this](exception_ptr eptr) {
      try {
        rethrow_exception(eptr);
      }
      catch (const hawk::Filesystem_error& e) {
        emit error_occured(e);
      }
    };
  }
  data = make_unique<DirectoryWidgetData>(w, c, cc, ts, eh, this);

  //  setAlternatingRowColors(true);
  setUniformItemSizes(true);
  setModel(&data->model);
  setIconSize(QSize(18, 18));
  setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
  setMinimumWidth(100);

  setDropIndicatorShown(true);
  setDragDropMode(QAbstractItemView::DropOnly);

  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  QPalette p = palette();
  p.setColor(QPalette::Highlight, Qt::lightGray);
  setPalette(p);

  connect(this, &DirectoryWidget::clicked, [this](const QModelIndex& idx) {
    hawk::Path p;
    {
      lock_guard<mutex> m{data->m};
      p = data->dirpath /
          data->model.data().dir.sorted_access().name(idx.row());
    }
    emit item_clicked(p);
  });
}

void DirectoryWidget::cancel_loading() {}

void DirectoryWidget::dropEvent(QDropEvent* event)
{
  const QMimeData* data = event->mimeData();
  if (!data->hasUrls())
    return;

  QList<QUrl> urls = data->urls();
  QMenu menu;
  hawk::Path dst = this->data->get_dirpath();

  for (const QUrl& url : urls) {
    qDebug() << "url" << url.toLocalFile();
  }

  menu.addAction("Copy here",
                 [&] { emit io_request(FileOperation::cp, urls, dst); });
  menu.addAction("Move here",
                 [&] { emit io_request(FileOperation::mv, urls, dst); });
  //  menu.addAction("Link here",
  //                 [&] { emit io_request(FileOperation::ln, urls, dst); });

  menu.exec(mapToGlobal(event->pos()));
}

void DirectoryWidget::mousePressEvent(QMouseEvent* event)
{
  QListView::mousePressEvent(event);

  QModelIndex idx = indexAt(event->pos());
  if (!idx.isValid()) {
    const hawk::Path p = path();
    auto found = data->cursors.find(p);
    if (found.second)
      emit item_clicked(p / found.first.filename());
    else {
      const hawk::Path filename =
          model()->index(0, 0).data().toString().toStdString();
      emit item_clicked(p / filename);
    }
  }
}

void DirectoryWidget::set_path(const hawk::Path& dirpath, unsigned page)
{
  lock_guard<mutex> lk{data->m};
  data->model.updateDirectory({});

  data->dirpath = dirpath;
  data->page = page;

  if (dirpath.empty())
    return;

  set_path_tasks(dirpath, page, TaskPriorty::setPath_load_dcache);
}

void DirectoryWidget::reload_path()
{
  lock_guard<mutex> lk{data->m};
  data->model.updateDirectory({});

  data->ts->run_noint_blocking(make_unique<Discard_dcache_task>(
      TaskPriorty::discard, data->cache, data->dirpath));
  data->ts->run_noint_blocking(make_unique<Discard_scache_task>(
      TaskPriorty::discard, data->cache, data->dirpath));

  if (data->dirpath.empty())
    return;

  set_path_tasks(data->dirpath, data->page,
                 TaskPriorty::reloadPath_load_dcache);
}

void DirectoryWidget::set_cursor(int row)
{
  QModelIndex idx = data->model.index(row, 0);
  setCurrentIndex(idx);
  scrollTo(idx, QAbstractItemView::PositionAtCenter);
}

void DirectoryWidget::set_filters(const hawk::Directory_data::Filters& fs,
                                  size_t id)
{
  lock_guard<mutex> lk{data->m};
  data->filters = fs;
  data->filters_id = id;
}

void DirectoryWidget::release() { data->release(); }

void DirectoryWidget::busy() { data->wspinner.start(); }

void DirectoryWidget::ready() { data->wspinner.stop(); }

void DirectoryWidget::set_path_tasks(const hawk::Path& dirpath, unsigned page,
                                     TaskPriorty set_path_priority)
{
  auto load_dcache = make_unique<Load_dcache_task>(
      set_path_priority, data->cache, data->wd, data->filters, dirpath, page);
  auto sort_dir = make_unique<Sort_task<Sort_natural>>(
      TaskPriorty::sort, data->cache, dirpath, page);
  auto update_model = make_unique<Update_dirmodel_task>(
      set_path_priority, data->cache, data->model, dirpath, page);
  auto set_cursor = make_unique<Set_cursor_task>(
      TaskPriorty::set_cursor, data->cache, data->cursors, dirpath, page);

  connect(update_model.get(), &Update_dirmodel_task::modelUpdated, &data->model,
          &DirectoryModel::updateDirectory);
  connect(update_model.get(), &Update_dirmodel_task::viewReady, this,
          &DirectoryWidget::ready);
  connect(set_cursor.get(), &Set_cursor_task::cursorSet, this,
          &DirectoryWidget::set_cursor);

  //  disconnect(verticalScrollBar(), nullptr, nullptr, nullptr);
  verticalScrollBar()->setValue(0);
  busy();

  data->ts->run(std::move(load_dcache), std::move(sort_dir),
                std::move(update_model), std::move(set_cursor));
}
