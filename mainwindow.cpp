#include "mainwindow.h"
#include "QTabWidget"
#include "advancedsearchresults.h"
#include "columngroup.h"
#include "directorysplit.h"
#include "fileoperation.h"
#include "fileoperations.h"
#include "ui_mainwindow.h"
#include "utils.h"
#include <QDebug>
#include <QLabel>
#include <QPushButton>
#include <QTabBar>
#include <list>
#include <unistd.h>

using namespace std;

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow{parent}, ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  m_tab_info = new QLabel(this);
  ui->statusbar->addPermanentWidget(m_tab_info);

  connect(ui->tabWidget, &QTabWidget::currentChanged, [this](int index) {
    disconnect(m_tab_info_conn);
    ColumnGroup* cg = static_cast<DirectorySplit*>(ui->tabWidget->widget(index))
                          ->columngroup();
    m_tab_info_conn = connect(cg, &ColumnGroup::update_dirinfo,
                              [this](DirInfo i) { set_tab_info(i); });
    set_tab_info(cg->dirinfo());
  });

  connect(ui->tabWidget, &QTabWidget::tabCloseRequested, [this](int index) {
    QWidget* w = ui->tabWidget->widget(index);
    ui->tabWidget->removeTab(index);
    w->deleteLater();

    update_tab_names();

    if (ui->tabWidget->count() == 0)
      close();
  });

  m_ops = new FileOperations(this);

  char working_directory[PATH_MAX];
  if (!getcwd(working_directory, PATH_MAX))
    strcpy(working_directory, "/");

  add_directory_tab(working_directory);
}

MainWindow::~MainWindow() { delete ui; }

int MainWindow::add_directory_tab(const hawk::Path& dirpath)
{
  DirectorySplit* ds = new DirectorySplit();
  init_directory_split(ds);
  int index = ui->tabWidget->addTab(ds, dirpath.filename().c_str());

  ds->set_path(dirpath);

  return index;
}

int MainWindow::add_directory_tab_with_cursor(const hawk::Path& p)
{
  DirectorySplit* ds =
      new DirectorySplit{p.parent_path(), Cursor(0, 0, p.filename()), this};
  init_directory_split(ds);
  int index = ui->tabWidget->addTab(ds, p.parent_path().c_str());

  ds->set_path(p.parent_path());

  return index;
}

void MainWindow::init_directory_split(DirectorySplit* ds)
{
  connect(ds, &DirectorySplit::new_tab_request, this,
          &MainWindow::add_directory_tab);
  connect(ds, &DirectorySplit::io_request,
          [this](FileOperation op, const QList<QUrl>& srcs,
                 const hawk::Path& dst) { m_ops->add(op, srcs, dst); });
  connect(ds, &DirectorySplit::advanced_search,
          [this](const hawk::Path& base, const AdvancedSearch::Criteria& cr) {
            AdvancedSearchResults* res = new AdvancedSearchResults(this);
            connect(res, &AdvancedSearchResults::new_tab_with_cursor_request,
                    this, &MainWindow::add_directory_tab_with_cursor);
            res->setWindowTitle(QString("Search in %1").arg(base.c_str()));
            res->show();

            AdvancedSearch* s = new AdvancedSearch(base, cr, res);
            connect(s, &AdvancedSearch::searchFinished, res,
                    &AdvancedSearchResults::setResults);
            s->search();
          });
  connect(ds, &DirectorySplit::path_changed,
          [this, ds](const hawk::Path&) { update_tab_names(); });
}

namespace {

struct Node {
  map<hawk::Path, Node> ns;
  int n = 0;
};

using Outputs = map<hawk::Path, vector<int>>;
using Paths = vector<hawk::Path>;
using Names = vector<QString>;
using Indices = vector<int>;

Node create_tree(const Paths& ps)
{
  Node root;
  for (const hawk::Path& p : ps) {
    Node* n = &root;
    for (auto it = p.rbegin(); it != p.rend(); ++it) {
      auto res = n->ns.insert({(*it).front(), {}});
      ++res.first->second.n;
      n = &((*res.first).second);
    }
  }

  return root;
}

Outputs create_outputs(const Paths& ps)
{
  Outputs os;
  for (int i = 0; i < (int)ps.size(); i++)
    os[ps[i]].push_back(i);

  return os;
}

void write_name(const hawk::Path& p, const Indices& is, Names& ns)
{
  if (is.size() == 1) {
    ns[is.front()] = p.c_str();
    return;
  }

  int n = 1;
  for (int idx : is)
    ns[idx] = QString("[%1] %2").arg(QString::number(n++)).arg(p.c_str());
}

Names generate_names(const vector<hawk::Path>& ps)
{
  Node root = create_tree(ps);
  Outputs os = create_outputs(ps);
  Names names(ps.size());

  for (const auto& o : os) {
    Node* n = &root;
    const hawk::Path& p = o.first;
    const Indices& is = o.second;
    const int sz = is.size();

    auto it = p.rbegin();
    for (; it != p.rend(); ++it) {
      n = &n->ns[(*it).front()];
      if (n->n == sz)
        break;
    }

    if (it == p.rend())
      write_name(p, is, names);
    else
      write_name(*it, is, names);
  }

  return names;
}

} // unnamed-namespace

void MainWindow::update_tab_names()
{
  vector<hawk::Path> ps;
  for (int i = 0; i < ui->tabWidget->count(); i++) {
    ps.push_back(
        static_cast<DirectorySplit*>(ui->tabWidget->widget(i))->dirpath());
  }

  auto names = generate_names(ps);
  for (int i = 0; i < (int)names.size(); i++)
    ui->tabWidget->setTabText(i, names[i]);
}

DirectorySplit* MainWindow::current_directory_split()
{
  return static_cast<DirectorySplit*>(ui->tabWidget->currentWidget());
}

void MainWindow::set_tab_info(const DirInfo& i)
{
  char buf[20];
  m_tab_info->setText(QString("%1 item(s) | %2 free / %3")
                          .arg(QString::number(i.items))
                          .arg(human_readable_filesize(i.freespace, buf))
                          .arg(human_readable_filesize(i.capacity, buf)));
}

void MainWindow::on_actionReload_triggered()
{
  current_directory_split()->reload_path();
}

void MainWindow::on_actionGo_back_triggered()
{
  DirectorySplit* ds = current_directory_split();
  ds->set_path(ds->dirpath().parent_path());
}

void MainWindow::on_actionOpen_new_tab_triggered()
{
  ui->tabWidget->setCurrentIndex(
      add_directory_tab(current_directory_split()->dirpath()));
}

void MainWindow::on_actionFile_operations_triggered() { m_ops->show(); }

void MainWindow::on_actionSearch_triggered()
{
  current_directory_split()->show_searchbar();
}

void MainWindow::on_actionAdvanced_search_triggered()
{
  current_directory_split()->show_advanced_search();
}

void MainWindow::on_actionChange_location_triggered()
{
  current_directory_split()->show_pathbar_edit();
}

void MainWindow::on_actionFilter_triggered()
{
  current_directory_split()->show_filterbar();
}
