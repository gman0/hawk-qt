#include "advancedsearchresults.h"
#include "ui_advancedsearchresultswindow.h"
#include <QCloseEvent>
#include <QDebug>
#include <QLabel>
#include <QMenu>
#include <QVBoxLayout>
#include <hawk/Path.h>

AdvancedSearchResults::AdvancedSearchResults(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::AdvancedSearchResultsWindow),
      m_spinner{new WaitingSpinnerWidget(this)}
{
  ui->setupUi(this);
  m_spinner->start();

  ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);

  connect(ui->treeWidget, &QTreeWidget::customContextMenuRequested,
          [this](const QPoint& pos) {
            QTreeWidgetItem* item = ui->treeWidget->itemAt(pos);
            if (!item)
              return;

            QMenu m;

            if (item->childCount() == 0)
              m.addAction("Open");

            m.addAction("Open in new tab", [this, item] {
              emit new_tab_with_cursor_request(hawk::Path{
                  item->data(0, Qt::UserRole).toString().toStdString()});
            });

            m.exec(ui->treeWidget->mapToGlobal(pos));
          });

  connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked,
          [](QTreeWidgetItem* item) {
            if (item->childCount() == 0)
              qDebug() << item->data(0, Qt::UserRole).toString();
          });
}

AdvancedSearchResults::~AdvancedSearchResults() { delete ui; }

void AdvancedSearchResults::setResults(const QList<QByteArray>& results)
{
  QTreeWidget* tree = ui->treeWidget;

  QTreeWidgetItem* topLevelItem = nullptr;
  for (const QByteArray& r : results) {
    auto split = r.split('/');

    if (tree->findItems(split[0], Qt::MatchFixedString).isEmpty()) {
      topLevelItem = new QTreeWidgetItem;
      topLevelItem->setText(0, split[0]);
      topLevelItem->setData(0, Qt::UserRole, r);
      tree->addTopLevelItem(topLevelItem);
    }

    QTreeWidgetItem* parentItem = topLevelItem;

    for (int i = 1; i < split.size() - 1; i++) {
      bool alreadyParent = false;
      for (int j = 0; j < parentItem->childCount(); j++) {
        if (split[i] == parentItem->child(j)->text(0)) {
          alreadyParent = true;
          parentItem = parentItem->child(j);
          break;
        }
      }

      if (!alreadyParent) {
        parentItem = new QTreeWidgetItem(parentItem);
        parentItem->setText(0, split[i]);
        parentItem->setData(0, Qt::UserRole, r);
      }
    }

    QTreeWidgetItem* childItem = new QTreeWidgetItem(parentItem);
    childItem->setText(0, split.last());
    childItem->setData(0, Qt::UserRole, r);
  }

  ui->treeWidget->expandAll();

  m_spinner->stop();
}

void AdvancedSearchResults::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}
