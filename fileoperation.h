#ifndef FILEOPERATION_H
#define FILEOPERATION_H

enum class FileOperation { cp, mv, rm, ln };

#endif // FILEOPERATION_H
