#ifndef DIRECTORYWIDGET_H
#define DIRECTORYWIDGET_H

#include "directorywidgetdata.h"
#include "fileoperation.h"
#include <QList>
#include <QListView>
#include <QUrl>
#include <memory>

class DirectoryWidget : public QListView {
private:
  Q_OBJECT

  std::unique_ptr<DirectoryWidgetData> data;

public:
  DirectoryWidget(hawk::Watchdog& w, Cache& c, CursorCache& cc,
                  hawk::Tasking* ts, QWidget* parent = nullptr);

  DirectoryWidget(DirectoryWidget&&) = delete;
  DirectoryWidget& operator=(DirectoryWidget&&) = delete;

  void cancel_loading();

protected:
  void dropEvent(QDropEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;

signals:
  void io_request(FileOperation op, const QList<QUrl>& srcs,
                  const hawk::Path& dst);
  void item_clicked(const hawk::Path& p);

  void error_occured(hawk::Filesystem_error e);

public slots:
  void set_path(const hawk::Path& dirpath, unsigned page);
  void reload_path();
  void set_cursor(int row);
  void set_filters(const hawk::Directory_data::Filters& fs, size_t id);

  void release();

  void busy();
  void ready();

  const hawk::Path& path() const { return data->dirpath; }
  unsigned page() const { return data->page; }

private:
  void set_path_tasks(const hawk::Path& dirpath, unsigned page,
                      TaskPriorty set_path_priority);
  void reload_path_tasks();
};

#endif // DIRECTORYWIDGET_H
