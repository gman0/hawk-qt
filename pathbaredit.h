#ifndef PATHBAREDIT_H
#define PATHBAREDIT_H

#include "focuslineedit.h"
#include "pathcompletionmodel.h"

class PathBarEdit : public FocusLineEdit {
private:
  Q_OBJECT

  QCompleter* m_completer;
  PathCompletionModel* m_model;

public:
  PathBarEdit(QWidget* parent = nullptr);

  void setCompleter(QCompleter* completer);
  QCompleter* completer() const;

public slots:
  void set_path(const hawk::Path& dirpath);

protected:
  void keyPressEvent(QKeyEvent* event) override;
  void focusInEvent(QFocusEvent* event) override;
  void focusOutEvent(QFocusEvent* event) override;

private slots:
  void insertCompletion(const QString& completion);

private:
  QString completionPrefix();
};

#endif // PATHBAREDIT_H
