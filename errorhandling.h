#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H

#include <hawk/Filesystem.h>

class QWidget;

void display_filesystem_error(const hawk::Filesystem_error& e);

#endif // ERRORHANDLING_H
