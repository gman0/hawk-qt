#ifndef NOPREVIEW_H
#define NOPREVIEW_H

#include <QWidget>

namespace Ui {
class NoPreview;
}

class NoPreview : public QWidget {
  Q_OBJECT

public:
  explicit NoPreview(QWidget* parent = 0);
  ~NoPreview();

private:
  Ui::NoPreview* ui;
};

#endif // NOPREVIEW_H
