#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include "cache.h"
#include "cursorcache.h"
#include <QMimeDatabase>
#include <QStackedWidget>
#include <QWidget>
#include <chrono>
#include <hawk/Path.h>
#include <hawk/Tasking.h>
#include <hawk/watchdog/Watchdog.h>
#include <iostream>

class DirectoryWidget;

class PreviewWidget : public QStackedWidget {
private:
  Q_OBJECT

  hawk::Path m_path;
  hawk::Tasking m_ts;
  hawk::Watchdog& m_wd;
  Cache& m_cache;
  CursorCache& m_cursors;

  QMimeDatabase m_mdb;

public:
  PreviewWidget(hawk::Watchdog& wd, Cache& cache, CursorCache& cursors,
                QWidget* parent = nullptr);

  DirectoryWidget* directory() const;

public slots:
  void set_path(const hawk::Path& p);

  void show_contents(QString html);
  void show_image(QImage* img);
  void show_no_preview();
  void show_file_info();

signals:
  void error_occured(hawk::Filesystem_error e);
};

#endif // PREVIEWWIDGET_H
