#ifndef ELIDEDLABEL_H
#define ELIDEDLABEL_H

#include <QLabel>

class ElidedLabel : public QLabel {
private:
  Q_OBJECT
  Q_PROPERTY(Qt::TextElideMode elideMode READ elideMode WRITE setElideMode)

  Qt::TextElideMode m_mode;
  QString m_text;

public:
  explicit ElidedLabel(QWidget* parent = nullptr,
                       Qt::WindowFlags f = Qt::WindowFlags());
  ElidedLabel(const QString& text, QWidget* parent = nullptr,
              Qt::WindowFlags f = Qt::WindowFlags());

  Qt::TextElideMode elideMode() const;
  void setElideMode(Qt::TextElideMode mode);

  QSize minimumSizeHint() const override;

  QString text() const;

public slots:
  void setText(const QString& text);

protected:
  //  void resizeEvent(QResizeEvent* event);

private:
  void updateElidedText(int width);
  void updateElidedText();
};

#endif // ELIDEDLABEL_H
