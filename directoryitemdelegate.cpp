#include "directoryitemdelegate.h"
#include "directorymodel.h"
#include <QLineEdit>
#include <hawk/Filesystem.h>

DirectoryItemDelegate::DirectoryItemDelegate(QObject* parent)
    : QStyledItemDelegate{parent}
{
}

QWidget* DirectoryItemDelegate::createEditor(QWidget* parent,
                                             const QStyleOptionViewItem&,
                                             const QModelIndex&) const
{
  QLineEdit* editor = new QLineEdit{parent};
  editor->setFrame(false);

  return editor;
}

void DirectoryItemDelegate::setEditorData(QWidget* editor,
                                          const QModelIndex& index) const
{
  QString value = index.model()->data(index, Qt::DisplayRole).toString();
  QLineEdit* line = static_cast<QLineEdit*>(editor);
  line->setText(value);
}

void DirectoryItemDelegate::setModelData(QWidget* editor,
                                         QAbstractItemModel* model,
                                         const QModelIndex& index) const
{
  QLineEdit* line = static_cast<QLineEdit*>(editor);

  const hawk::Path& base =
      static_cast<DirectoryModel*>(model)->data().dir.path();
  hawk::rename(base / index.data().toString().toStdString(),
               line->text().toStdString());

  model->setData(index, line->text(), Qt::EditRole);
}

void DirectoryItemDelegate::updateEditorGeometry(
    QWidget* editor, const QStyleOptionViewItem& option,
    const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
  QStyledItemDelegate::updateEditorGeometry(editor, option, index);
}
