#include "newdirectorywindow.h"
#include "ui_newdirectorywindow.h"
#include <QCloseEvent>
#include <hawk/Filesystem.h>

NewDirectoryWindow::NewDirectoryWindow(const hawk::Path& base, QWidget* parent)
    : QMainWindow(parent), ui(new Ui::NewDirectoryWindow), m_base{base}
{
  ui->setupUi(this);

  ui->baseLabel->setText(base.c_str());

  connect(ui->createBtn, &QPushButton::clicked, [this] {
    int err;
    hawk::create_directory(m_base / ui->nameEdit->text().toStdString(), err);
  });
}

NewDirectoryWindow::~NewDirectoryWindow() { delete ui; }

void NewDirectoryWindow::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}
