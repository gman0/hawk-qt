#ifndef IOEXISTSWINDOW_H
#define IOEXISTSWINDOW_H

#include <QMainWindow>

namespace Ui {
class IOExistsWindow;
}

class IOExistsWindow : public QMainWindow {
  Q_OBJECT

public:
  IOExistsWindow(QString from, QString to, QWidget* parent = nullptr);
  ~IOExistsWindow();

protected:
  void closeEvent(QCloseEvent* event);

signals:
  void overwrite(bool always);
  void rename(bool always);
  void cancel();

private:
  Ui::IOExistsWindow* ui;
};

#endif // IOEXISTSWINDOW_H
