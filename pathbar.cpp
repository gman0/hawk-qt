#include "pathbar.h"
#include <QAbstractSlider>
#include <QDebug>
#include <QScrollBar>
#include <QTimer>
#include <QWheelEvent>

PathBar::PathBar(QWidget* parent) : QWidget{parent}
{
  // Main layout
  // [scroll-to-start button] | path buttons | [scroll-to-end button]
  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSpacing(5);

  // scroll-to-start button
  m_scrollStartBtn = new QToolButton(this);
  m_scrollStartBtn->setArrowType(Qt::LeftArrow);
  m_scrollStartBtn->setAutoRepeat(true);
  m_scrollStartBtn->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
  connect(m_scrollStartBtn, &QToolButton::clicked, this,
          &PathBar::scrollbtn_click);
  layout->addWidget(m_scrollStartBtn);

  // This will contain both the crumbs (buttons) and separators
  m_crumbs = new QWidget(this);
  m_crumbs->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

  // Layout for the crumbs container
  m_crumbs_layout = new QHBoxLayout(m_crumbs);
  m_crumbs_layout->setContentsMargins(0, 0, 0, 0);
  m_crumbs_layout->setSpacing(10);
  m_crumbs_layout->setSizeConstraint(QLayout::SetFixedSize);

  // Scroll area for the crumbs
  m_scrollArea = new QScrollArea(this);
  m_scrollArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  m_scrollArea->setFrameShape(QFrame::NoFrame);
  m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  m_scrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
  m_scrollArea->verticalScrollBar()->setDisabled(true);
  m_scrollArea->setWidget(m_crumbs);
  m_scrollArea->setFixedHeight(19); // TODO: fix this
  connect(m_scrollArea->horizontalScrollBar(), &QScrollBar::valueChanged, this,
          &PathBar::setScrollButtonEnable);
  layout->addWidget(m_scrollArea, 1);

  // scroll-to-end button
  m_scrollEndBtn = new QToolButton(this);
  m_scrollEndBtn->setArrowType(Qt::RightArrow);
  m_scrollEndBtn->setAutoRepeat(true);
  m_scrollEndBtn->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
  connect(m_scrollEndBtn, &QToolButton::clicked, this,
          &PathBar::scrollbtn_click);
  layout->addWidget(m_scrollEndBtn);

  setCursor(Qt::IBeamCursor);
}

PathBar::~PathBar() { clear(); }

void PathBar::resizeEvent(QResizeEvent* event)
{
  QWidget::resizeEvent(event);
  update_scrollbar_visibility();
}

void PathBar::wheelEvent(QWheelEvent* event)
{
  QWidget::wheelEvent(event);

  QAbstractSlider::SliderAction action = QAbstractSlider::SliderNoAction;
  int d = event->angleDelta().y();

  if (d > 0) {
    if (m_scrollStartBtn->isEnabled())
      action = QAbstractSlider::SliderSingleStepSub;
  }
  else if (d < 0) {
    if (m_scrollEndBtn->isEnabled())
      action = QAbstractSlider::SliderSingleStepAdd;
  }

  m_scrollArea->horizontalScrollBar()->triggerAction(action);
}

void PathBar::mouseReleaseEvent(QMouseEvent* event)
{
  QWidget::mouseReleaseEvent(event);

  if (event->button() != Qt::LeftButton)
    return;

  PathBarButton* btn =
      qobject_cast<PathBarButton*>(childAt(event->x(), event->y()));
  if (btn) {
    m_scrollArea->ensureWidgetVisible(btn, 0);
    emit clicked(btn->path());
  }
}

void PathBar::mouseDoubleClickEvent(QMouseEvent* event)
{
  QWidget::mouseDoubleClickEvent(event);
  emit doubleClicked();
}

/*
void PathBar::enterEvent(QEvent* event)
{
  QWidget::enterEvent(event);

  QLabel* ibeam_label = new QLabel("|");
  ibeam_label->setStyleSheet("font-weight: bold");
  m_crumbs_layout->addWidget(ibeam_label);
}

void PathBar::leaveEvent(QEvent* event)
{
  QWidget::leaveEvent(event);

  QLayoutItem* item = m_crumbs_layout->takeAt(m_crumbs_layout->count() - 1);
  delete item->widget();
  delete item;
}
*/

void PathBar::set_path(const hawk::Path& dirpath)
{
  if (dirpath == m_path)
    return;

  m_path = dirpath;

  setUpdatesEnabled(false);

  clear();

  const std::string& str = dirpath.string();
  using Size = std::string::size_type;

  add_crumb("/");

  if (str.size() == 1) {
    setUpdatesEnabled(true);
    return;
  }

  Size idx = 1;
  while (idx < str.size()) {
    add_separator();

    Size found = str.find('/', idx);
    if (found == std::string::npos)
      idx = str.size();
    else
      idx = found;

    add_crumb(str.substr(0, idx));
    idx++;
  }

  setUpdatesEnabled(true);
}

void PathBar::crumb_click(const hawk::Path& dirpath) { emit clicked(dirpath); }

void PathBar::scrollbtn_click()
{
  QToolButton* btn = static_cast<QToolButton*>(sender());
  QAbstractSlider::SliderAction action = QAbstractSlider::SliderNoAction;

  if (btn == m_scrollStartBtn)
    action = QAbstractSlider::SliderSingleStepSub;
  else if (btn == m_scrollEndBtn)
    action = QAbstractSlider::SliderSingleStepAdd;

  m_scrollArea->horizontalScrollBar()->triggerAction(action);
}

void PathBar::setScrollButtonEnable(int value)
{
  if (m_crumbs_layout->sizeHint().width() > width()) {
    QScrollBar* sb = m_scrollArea->horizontalScrollBar();
    m_scrollStartBtn->setEnabled(value != sb->minimum());
    m_scrollEndBtn->setEnabled(value != sb->maximum());
  }
}

void PathBar::add_crumb(const std::string& str)
{
  PathBarButton* btn = new PathBarButton(str);
  m_crumbs_layout->addWidget(btn);
}

void PathBar::add_separator()
{
  QLabel* sep = new QLabel(QString::fromUtf8("\u27A4"));
  m_crumbs_layout->addWidget(sep);
}

void PathBar::clear()
{
  QLayoutItem* i;
  while ((i = m_crumbs_layout->takeAt(0))) {
    delete i->widget();
    delete i;
  }
}

void PathBar::update_scrollbar_visibility()
{
  QTimer::singleShot(0, this, &PathBar::set_scrollbar_visibility);
}

void PathBar::set_scrollbar_visibility()
{
  bool visible = m_crumbs->sizeHint().width() > width();
  m_scrollStartBtn->setVisible(visible);
  m_scrollEndBtn->setVisible(visible);

  if (visible) {
    QScrollBar* sb = m_scrollArea->horizontalScrollBar();
    int val = sb->value();

    m_scrollStartBtn->setEnabled(val != sb->minimum());
    m_scrollEndBtn->setEnabled(val != sb->maximum());
  }
}
