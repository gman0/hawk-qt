#ifndef IOCONTROL_H
#define IOCONTROL_H

#include "fileoperation.h"
#include <QWidget>
#include <hawk/Path.h>
#include <hawk/io/IO_task.h>

namespace Ui {
class IOControl;
}

class IOControl : public QWidget {
  Q_OBJECT

  Ui::IOControl* ui;
  FileOperation m_type;

public:
  explicit IOControl(FileOperation type, QWidget* parent = 0);
  ~IOControl();

signals:
  void cont_clicked();
  void cancel_clicked();

public slots:
  void set_task_info(const hawk::IO_task::Total_progress& tp);
  void set_status_info(hawk::IO_task::Status st);
  void set_file_info(const hawk::IO_task::File_progress& fp);
};

#endif // IOCONTROL_H
