#ifndef REMOVECONFIRMWINDOW_H
#define REMOVECONFIRMWINDOW_H

#include <QAbstractItemModel>
#include <QMainWindow>
#include <hawk/Path.h>

namespace Ui {
class RemoveConfirmWindow;
}

class RemoveConfirmWindow : public QMainWindow {
  Q_OBJECT

public:
  RemoveConfirmWindow(const hawk::Path& base, const QModelIndexList& idxs,
                      QWidget* parent = nullptr);
  ~RemoveConfirmWindow();

protected:
  void closeEvent(QCloseEvent* event);

signals:
  void removeConfirmed();

private:
  Ui::RemoveConfirmWindow* ui;
};

#endif // REMOVECONFIRMWINDOW_H
