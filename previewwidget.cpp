#include "previewwidget.h"
#include "directorywidget.h"
#include "errorhandling.h"
#include "fileinfopreview.h"
#include "imagepreview.h"
#include "nopreview.h"
#include <QDebug>
#include <QHBoxLayout>
#include <QImageReader>
#include <QMessageBox>
#include <QSpacerItem>
#include <QTextBrowser>

using namespace std;

PreviewWidget::PreviewWidget(hawk::Watchdog& wd, Cache& cache,
                             CursorCache& cursors, QWidget* parent)
    : QStackedWidget(parent), m_ts(std::chrono::milliseconds{500},
                                   [this](std::exception_ptr eptr) {
                                     try {
                                       rethrow_exception(eptr);
                                     }
                                     catch (const hawk::Filesystem_error& e) {
                                       // emit error_occured(e);
                                     }
                                     directory()->ready();
                                     directory()->release();
                                     // TODO: clear widget!
                                     show_no_preview();
                                   }),
      m_wd{wd}, m_cache{cache}, m_cursors{cursors}
{
  setMinimumWidth(100);
  setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Ignored);

  addWidget(new FileInfoPreview(this));
  addWidget(new DirectoryWidget{m_wd, m_cache, m_cursors, &m_ts, this});
  addWidget(new QTextBrowser(this));
  addWidget(new ImagePreview(this));
  addWidget(new NoPreview(this));

  setCurrentIndex(0);
}

DirectoryWidget* PreviewWidget::directory() const
{
  return static_cast<DirectoryWidget*>(widget(1));
}

void PreviewWidget::set_path(const hawk::Path& p)
{
  m_path = p;

  if (p.empty()) {
    show_no_preview();
    return;
  }

  hawk::Stat st = hawk::symlink_status(p);
  if (!S_ISREG(st.st_mode) && !S_ISDIR(st.st_mode) && !S_ISLNK(st.st_mode)) {
    show_file_info();
    return;
  }

  setEnabled(true);

  QMimeType type = m_mdb.mimeTypeForFile(p.c_str());

  if (type.inherits("inode/directory")) {
    setCurrentIndex(1);
    directory()->set_path(p, 0);
  }
  else if (type.inherits("text/plain")) {
    auto textprev_task =
        make_unique<Text_preview_task>(TaskPriorty::preview, p);
    connect(textprev_task.get(), &Text_preview_task::contentsReady, this,
            &PreviewWidget::show_contents);

    m_ts.run(std::move(textprev_task));
  }
  else if (!QImageReader::imageFormat(p.c_str()).isEmpty()) {
    auto imgprev_task = make_unique<Load_image_task>(TaskPriorty::preview, p);
    connect(imgprev_task.get(), &Load_image_task::imageReady, this,
            &PreviewWidget::show_image);
    m_ts.run(std::move(imgprev_task));
  }
  else {
    // TODO: reset other widgets!
    directory()->release();
    show_file_info();
  }
}

void PreviewWidget::show_contents(QString html)
{
  setCurrentIndex(2);
  QTextBrowser* w = static_cast<QTextBrowser*>(widget(2));
  w->setHtml(html);
}

void PreviewWidget::show_image(QImage* img)
{
  if (img->isNull()) {
    show_file_info();
    return;
  }

  setCurrentIndex(3);
  ImagePreview* w = static_cast<ImagePreview*>(widget(3));
  w->set_image(img);
}

void PreviewWidget::show_no_preview()
{
  setCurrentIndex(4);
  setDisabled(true);
}

void PreviewWidget::show_file_info()
{
  setCurrentIndex(0);
  FileInfoPreview* fprev = static_cast<FileInfoPreview*>(widget(0));
  fprev->set_path(m_path);
}
