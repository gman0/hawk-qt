#ifndef APPLY_H
#define APPLY_H

#include <QPixmap>
#include <hawk/Directory.h>
#include <hawk/Stat_cache.h>
#include <vector>

void apply_stat(const hawk::Path& base, hawk::Stat_cache::Data_raw_ptr sc,
                hawk::Directory::Static st, hawk::Static_idx idx);

void apply_thumbnail(const hawk::Path& base,
                     std::vector<std::pair<uint64_t, QImage>>& thumbs,
                     hawk::Directory::Static st, hawk::Static_idx idx);

#endif // APPLY_H
