#ifndef BULKRENAMEWINDOW_H
#define BULKRENAMEWINDOW_H

#include <QMainWindow>
#include <QModelIndexList>
#include <QStyledItemDelegate>
#include <ctime>
#include <hawk/Filesystem.h>

namespace Ui {
class BulkRenameWindow;
}

class BulkRenameDelegate : public QStyledItemDelegate {
private:
  Q_OBJECT

public:
  using QStyledItemDelegate::QStyledItemDelegate;
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;

  void setModelData(QWidget* editor, QAbstractItemModel* model,
                    const QModelIndex& index) const override;
};

class BulkRenameWindow : public QMainWindow {
private:
  Q_OBJECT

  friend class BulkRenameDelegate;

  Ui::BulkRenameWindow* ui;

  hawk::Path m_base;
  hawk::Path m_bulkfile_path;
  int m_bulkfile;
  int m_preamble_offset; // Used for skipping the first 3 lines
                         // when reading the file
  time_t m_bulkfile_timestamp;

public:
  BulkRenameWindow(const hawk::Path& base, const QModelIndexList& idxs,
                   QWidget* parent = nullptr);
  BulkRenameWindow(const BulkRenameWindow&) = delete;
  BulkRenameWindow& operator=(const BulkRenameWindow&) = delete;
  ~BulkRenameWindow();

  void create_bulkfile();

protected:
  void closeEvent(QCloseEvent* event);

private slots:
  void open_editor();
  void bulk_rename();

private:
  bool update_model_from_bulkfile();
  void update_bulkfile();
  void write_model_to_bulkfile(int col);
  void discard_bulkfile();
};

#endif // BULKRENAMEWINDOW_H
