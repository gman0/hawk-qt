#ifndef UTILS_H
#define UTILS_H

#include <QList>
#include <QUrl>
#include <chrono>
#include <hawk/Path.h>
#include <hawk/Region.h>
#include <iostream>
#include <vector>

std::ostream& operator<<(std::ostream& os, const hawk::Region& r);
std::ostream& operator<<(std::ostream& os, const hawk::Path& r);

std::vector<hawk::Path> to_paths_vector(const QList<QUrl>& srcs);

char* human_readable_filesize(double size, char* buf);
char* human_readable_time(std::chrono::seconds s, char* buf);

void open_file(const hawk::Path& p);

template <typename Fn, typename... Args>
auto measure_time(Fn&& f, Args&&... args)
{
  auto start = std::chrono::steady_clock::now();
  f(std::forward<Args>(args)...);
  auto end = std::chrono::steady_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
      .count();
}

#endif // UTILS_H
