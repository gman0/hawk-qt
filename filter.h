#ifndef FILTER_H
#define FILTER_H

#include <QRegExp>
#include <QString>
#include <hawk/Filesystem.h>

struct Filter_none {
  bool operator()(const dirent64* ent) const noexcept;
};

struct Filter_hidden {
  bool operator()(const dirent64* ent) const noexcept;
};

struct Filter_ndirectories {
  bool operator()(const dirent64* ent) const noexcept;
};

struct Filter_files_by_name {
  QString pattern;
  Filter_files_by_name(const QString& str) : pattern{str} {}
  bool operator()(const dirent64* ent) const noexcept;
};

struct Filter_files_by_name_regex {
  QRegExp rx;
  Filter_files_by_name_regex(const QString& pattern) : rx{pattern} {}
  bool operator()(const dirent64* ent) const noexcept;
};

#endif // FILTER_H
