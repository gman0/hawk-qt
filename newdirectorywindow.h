#ifndef NEWDIRECTORYWINDOW_H
#define NEWDIRECTORYWINDOW_H

#include <QMainWindow>
#include <hawk/Path.h>

namespace Ui {
class NewDirectoryWindow;
}

class NewDirectoryWindow : public QMainWindow {
  Q_OBJECT

  Ui::NewDirectoryWindow* ui;
  const hawk::Path m_base;

public:
  NewDirectoryWindow(const hawk::Path& base, QWidget* parent = nullptr);
  ~NewDirectoryWindow();

protected:
  void closeEvent(QCloseEvent* event);

private:
};

#endif // NEWDIRECTORYWINDOW_H
