#include "tasks.h"
#include "apply.h"
#include "sort.h"
#include "sortcriterium.h"
#include <QDebug>
#include <QImageReader>
#include <QMimeDatabase>
#include <QMimeType>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QSet>
#include <QSettings>
#include <cassert>

using namespace std;

/// Load_dcache_task

void Load_dcache_task::run()
{
  cout << "# Load_dcache_task:run()" << endl;

  auto data = m_cache.dcache.load(m_dirpath, m_page);
  if (!data) {
    data = m_cache.dcache.acquire();
    data->set_path(m_dirpath, m_page, m_filters);

    m_commit_data = std::move(data);
  }
}

void Load_dcache_task::commit() noexcept
{
  cout << "# Load_dcache_task:commit()" << endl;
  if (m_commit_data) {
    m_cache.dcache.store(m_commit_data, m_dirpath, m_page);
    m_wd.add_path(m_dirpath);
  }
}

/// Discard_dcache_task

void Discard_dcache_task::commit() noexcept
{
  cout << "# Discard_dcache_task:commit()" << endl;
  m_cache.dcache.discard(m_dirpath);
}

/// create sort task

std::unique_ptr<hawk::Tasking::Task> create_sort_task(TaskPriorty p, Cache& c,
                                                      const hawk::Path& dirpath,
                                                      unsigned page)
{
  SortCriterium cr =
      static_cast<SortCriterium>(QSettings().value("sort").toInt());

  switch (cr) {
  case SortCriterium::natural:
    return make_unique<Sort_task<Sort_natural>>(p, c, dirpath, page);
  case SortCriterium::natural_rev:
    return make_unique<Sort_task<Sort_natural_reverse>>(p, c, dirpath, page);
  case SortCriterium::size:
    return make_unique<Sort_task<Sort_size>>(p, c, dirpath, page);
  case SortCriterium::size_rev:
    return make_unique<Sort_task<Sort_size_reverse>>(p, c, dirpath, page);
  }

  return nullptr;
}

/// Load_scache_task

void Load_scache_task::run()
{
  cout << "# Load_scache_task:run()" << endl;
  auto ptr = m_cache.scache.load(m_dirpath, m_page);
  if (!ptr) {
    ptr = m_cache.scache.acquire();
    m_commit_data = ptr;
  }

  hawk::Directory dir{m_dirpath, m_page,
                      m_cache.dcache.find(m_dirpath, m_page)};

  auto st = dir.static_access();
  auto r = getregion();
  dir.apply(ptr->rs, r.low, r.high, [&](hawk::Static_idx idx) {
    lock_guard<mutex> lk{ptr->m};
    apply_stat(m_dirpath, ptr.get(), st, idx);
  });
}

void Load_scache_task::commit() noexcept
{
  cout << "# Load_scache_task:commit()" << endl;
  if (m_commit_data)
    m_cache.scache.store(m_commit_data, m_dirpath, m_page);
}

/// Discard_scache_task

void Discard_scache_task::commit() noexcept
{
  cout << "# Discard_scache_task:commit()" << endl;
  m_cache.scache.discard(m_dirpath);
}

/// Update_dirmodel_task

void Update_dirmodel_task::commit() noexcept
{
  cout << "# Update_dirmodel_task:commit()" << endl;

  auto scache = m_cache.scache.find(m_dirpath, m_page);
  if (!scache) {
    scache = m_cache.scache.acquire();
    m_cache.scache.store(scache, m_dirpath, m_page);
  }

  auto tcache = m_cache.tcache.find(m_dirpath, m_page);
  if (!tcache) {
    tcache = m_cache.tcache.acquire();
    m_cache.tcache.store(tcache, m_dirpath, m_page);
  }

  emit modelUpdated(
      Data{hawk::Directory(m_dirpath, m_page,
                           m_cache.dcache.find(m_dirpath, m_page)),
           std::move(scache), std::move(tcache)});
  emit viewReady();
}

/// Scanned_dirmodel_task

void Scanned_dirmodel_task::commit() noexcept
{
  cout << "# Scanned_dirmodel_task:commit()" << endl;
  emit scannedItems(m_from, m_to);
}

/// Unload_scache_task

void Unload_scache_task::commit() noexcept
{
  if (!m_scache)
    return;

  cout << "# Unload_scache_task:commit()" << endl;
  hawk::Region r;
  r.low = max(0, m_at - 1000);
  r.high = min((uint64_t)m_at + 1000, m_dir.size());

  m_dir.inv_apply(m_scache->rs, r.low, r.high, [&](hawk::Static_idx idx) {
    lock_guard<mutex> lk{m_scache->m};
    m_scache->map.erase(idx.value());
  });

  m_scache->rs.intersect(r);
}

/// Set_cursor_task

void Set_cursor_task::run()
{
  cout << "# Set_cursor_task:run()" << endl;
  hawk::Directory dir{m_dirpath, m_page,
                      m_cache.dcache.find(m_dirpath, m_page)};

  auto cursor = m_cursors.find(m_dirpath);
  auto sr = dir.sorted_access();

  if (!cursor.second) {
    m_row = 0;
    return;
  }

  const ino64_t inode = cursor.first.inode();

  if (cursor.first.row() < (int)sr.size() &&
      sr.inode(cursor.first.row()) == inode)
    m_row = cursor.first.row();
  else {
    const uint64_t sz = dir.size();
    const hawk::Path filename = cursor.first.filename();

    for (uint64_t i = 0; i < sz; i++) {
      hawk::soft_interruption_point();

      if (filename == sr.name(i)) {
        m_row = i;
        m_cursor = Cursor{(int)i, inode, cursor.first.filename()};

        return;
      }
    }

    m_row = 0;
  }
}

void Set_cursor_task::commit() noexcept
{
  cout << "# Set_cursor_task:commit()" << endl;

  if (m_cursor.is_valid())
    m_cursors.store(m_dirpath, m_cursor);

  emit cursorSet(m_row);
}

/// Text_preview_task

void Text_preview_task::run()
{
  QQmlComponent comp(&m_engine, ":/scripts/textpreview.qml");
  std::unique_ptr<QObject> comp_instance{comp.create()};

  QVariant ret;
  QVariant arg{m_path.c_str()};
  QMetaObject::invokeMethod(comp_instance.get(), "getFileContents",
                            Q_RETURN_ARG(QVariant, ret), Q_ARG(QVariant, arg));

  m_contents = ret.toString();
}

void Text_preview_task::commit() noexcept { emit contentsReady(m_contents); }

/// Load_image_task

void Load_image_task::run() { m_image->load(m_path.c_str()); }

void Load_image_task::commit() noexcept { emit imageReady(m_image.release()); }

namespace {

QSet<QByteArray> init_image_mimes()
{
  QSet<QByteArray> ms;
  for (const QByteArray& mime : QImageReader::supportedMimeTypes())
    ms << mime;

  return ms;
}

QSet<QByteArray> s_image_mimes = init_image_mimes();

} // unnamed-namespace

void Load_tcache_task::run()
{
  cout << "# Load_tcache_task:run()" << endl;
  auto ptr = m_cache.tcache.load(m_dirpath, m_page);
  if (!ptr)
    ptr = Thumbnail_cache::acquire();

  hawk::Directory dir{m_dirpath, m_page,
                      m_cache.dcache.find(m_dirpath, m_page)};
  auto st = dir.static_access();
  m_rs = [&] {
    lock_guard<mutex> m{ptr->m};
    return ptr->rs;
  }();
  hawk::Region target = getregion();

  QMimeDatabase mime_db;

  dir.apply(m_rs, target.low, target.high, [&](hawk::Static_idx idx) {
    const unsigned char type = st.type(idx);
    if (!hawk::is_regular_file(type) || hawk::is_symlink(type))
      return;

    if (s_image_mimes.contains(
            mime_db.mimeTypeForName(st.name(idx)).name().toLocal8Bit()))
      apply_thumbnail(m_dirpath, m_thumbs, st, idx);
  });
}

void Load_tcache_task::commit() noexcept
{
  auto ptr = m_cache.tcache.load(m_dirpath, m_page);
  if (!ptr) {
    ptr = Thumbnail_cache::acquire();
    m_cache.tcache.store(ptr, m_dirpath, m_page);
  }

  lock_guard<mutex> lk{ptr->m};

  for (auto& thumb : m_thumbs)
    ptr->map.insert(std::move(thumb));

  ptr->rs = m_rs;
}

void Discard_tcache_task::commit() noexcept
{
  cout << "# Discard_tcache_task:commit()" << endl;
  m_cache.tcache.discard(m_dirpath);
}

void Unload_tcache_task::commit() noexcept
{
  if (!m_tcache)
    return;

  cout << "# Unload_tcache_task:commit()" << endl;
  hawk::Region r;
  r.low = max(0, m_at - 1000);
  r.high = min((uint64_t)m_at + 1000, m_dir.size());

  m_dir.inv_apply(m_tcache->rs, r.low, r.high, [&](hawk::Static_idx idx) {
    lock_guard<mutex> lk{m_tcache->m};
    m_tcache->map.erase(idx.value());
  });

  m_tcache->rs.intersect(r);
}
