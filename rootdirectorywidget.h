#ifndef ROOTDIRECTORYWIDGET_H
#define ROOTDIRECTORYWIDGET_H

#include "directorywidgetdata.h"
#include "dirinfo.h"
#include "fileoperation.h"
#include <QTreeView>

class RootDirectoryWidget : public QTreeView {
private:
  Q_OBJECT

  DirectoryWidgetData data;

public:
  RootDirectoryWidget(hawk::Watchdog& w, Cache& c, CursorCache& cc,
                      QWidget* parent = nullptr);

public slots:
  void set_path(const hawk::Path& dirpath, unsigned page);
  void reload_path();
  void set_cursor(int row);
  void scan_at(int from, int to);
  void set_filters(const hawk::Directory_data::Filters& fs, size_t id);

  void release();

  void busy();
  void ready();

  DirInfo dirinfo() const;

  hawk::Path path() const;
  unsigned page() const;

protected:
  void contextMenuEvent(QContextMenuEvent* event) override;
  void dropEvent(QDropEvent* event) override;

  void keyPressEvent(QKeyEvent* event);

signals:
  void update_preview(const hawk::Path& p);
  void new_tab_request(const hawk::Path& dirpath);
  void io_request(FileOperation op, const QList<QUrl>& srcs,
                  const hawk::Path& dst);

  void error_occured(hawk::Filesystem_error e);

  void update_dirinfo(DirInfo i);

private:
  void set_path_tasks(const hawk::Path& dirpath, unsigned page,
                      TaskPriorty set_path_priority);

  void remove_selection(const QModelIndexList& idxs);

  void show_file_operations(const QMimeData* data, QPoint pos);
};

#endif // ROOTDIRECTORYWIDGET_H
