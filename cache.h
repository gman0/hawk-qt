#ifndef CACHE_H
#define CACHE_H

#include "thumbnailcache.h"
#include <hawk/Directory_cache.h>
#include <hawk/Stat_cache.h>

struct Cache {
  hawk::Directory_cache dcache;
  hawk::Stat_cache scache;
  Thumbnail_cache tcache;

  Cache(hawk::On_cache_free f) : dcache{1000000, std::move(f)} {}
};

#endif // CACHE_H
