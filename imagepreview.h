#ifndef IMAGEPREVIEW_H
#define IMAGEPREVIEW_H

#include "aspectratiopixmaplabel.h"
#include <QLabel>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>

class ImagePreview : public QWidget {
private:
  Q_OBJECT

  QVBoxLayout m_vbox;
  std::unique_ptr<QImage> m_img;
  AspectRatioPixmapLabel* m_imglabel;
  QLabel m_res;
  QLabel m_format;

public:
  explicit ImagePreview(QWidget* parent = nullptr);
  void set_image(QImage* img);
};

#endif // IMAGEPREVIEW_H
