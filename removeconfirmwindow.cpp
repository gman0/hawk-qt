#include "removeconfirmwindow.h"
#include "ui_removeconfirmwindow.h"

RemoveConfirmWindow::RemoveConfirmWindow(const hawk::Path& base,
                                         const QModelIndexList& idxs,
                                         QWidget* parent)
    : QMainWindow(parent), ui(new Ui::RemoveConfirmWindow)
{
  ui->setupUi(this);
  ui->baseLabel->setText(base.c_str());

  for (const QModelIndex& idx : idxs)
    ui->listWidget->addItem(idx.data().toString());

  connect(ui->removeBtn, &QPushButton::clicked,
          [&] { emit removeConfirmed(); });
}

RemoveConfirmWindow::~RemoveConfirmWindow() { delete ui; }

void RemoveConfirmWindow::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}
