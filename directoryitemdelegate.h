#ifndef DIRECTORYITEMDELEGATE_H
#define DIRECTORYITEMDELEGATE_H

#include <QStyledItemDelegate>

class DirectoryItemDelegate : public QStyledItemDelegate {
private:
  Q_OBJECT
public:
  explicit DirectoryItemDelegate(QObject* parent = nullptr);

  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;

  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor, QAbstractItemModel* model,
                    const QModelIndex& index) const override;

  void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

#endif // DIRECTORYITEMDELEGATE_H
