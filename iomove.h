#ifndef IOMOVE_H
#define IOMOVE_H

#include <QObject>
#include <hawk/io/IO_task_move.h>

class IOMove : public QObject, public hawk::IO_task_move {
  Q_OBJECT
public:
  IOMove(const QList<QUrl>& srcs, const hawk::Path& dst,
         QObject* parent = nullptr);

protected:
  void on_status_change(hawk::IO_task::Status st) noexcept override;
  void
  on_task_progress(const hawk::IO_task::Total_progress& tp) noexcept override;
  void on_file_progress(const File_progress& fp) noexcept override;

  void handle_error(const hawk::Filesystem_error& e, Target& t) noexcept;
  void handle_prepare_error(const hawk::Filesystem_error& e) noexcept;

signals:
  void status_changed(hawk::IO_task::Status st);
  void task_progressed(const hawk::IO_task::Total_progress& tp);
  void file_progressed(const hawk::IO_task::File_progress& fp);

  void error_occured(hawk::Filesystem_error e, hawk::IO_task::Target* t,
                     hawk::IO_task* parent);
};

#endif // IOMOVE_H
