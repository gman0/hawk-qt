#include "utils.h"
#include <QDebug>
#include <QDesktopServices>
#include <QProcess>
#include <cstdio>
#include <hawk/Filesystem.h>

using namespace hawk;

std::ostream& operator<<(std::ostream& os, const Region& r)
{
  os << '[' << r.low << ", " << r.high << ')';
  return os;
}

std::ostream& operator<<(std::ostream& os, const Path& r)
{
  os << '\'' << r.c_str() << '\'';
  return os;
}

std::vector<Path> to_paths_vector(const QList<QUrl>& srcs)
{
  std::vector<Path> ps;
  for (const QUrl& src : srcs)
    ps.push_back(src.toLocalFile().toStdString());

  return ps;
}

char* human_readable_filesize(double size, char* buf)
{
  int i = 0;
  const char* units[] = {"B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
  while (size > 1024) {
    size /= 1024;
    i++;
  }

  sprintf(buf, "%.*f %s", i, size, units[i]);
  return buf;
}

char* human_readable_time(std::chrono::seconds s, char* buf)
{
  using namespace std::chrono;
  using days = duration<int, std::ratio<86400>>;
  auto d = duration_cast<days>(s);
  s -= d;
  auto h = duration_cast<hours>(s);
  s -= h;
  auto m = duration_cast<minutes>(s);
  s -= m;

  char* offs = buf;

  if (d.count() > 0)
    offs += sprintf(offs, "%d days, ", d.count());

  if (h.count() > 0)
    offs += sprintf(offs, "%ld hours, ", h.count());

  if (m.count() > 0)
    offs += sprintf(offs, "%ld minutes, ", m.count());

  sprintf(offs, "%ld second(s)", s.count());

  return buf;
}

void open_file(const Path& p)
{
  if (hawk::is_executable_file(p))
    QProcess::startDetached(p.c_str());
  else
    QDesktopServices::openUrl(QUrl::fromLocalFile(p.c_str()));
}
