#ifndef SEARCHRESULTS_H
#define SEARCHRESULTS_H

#include "waitingspinnerwidget.h"
#include <QMainWindow>
#include <hawk/Path.h>

namespace Ui {
class AdvancedSearchResultsWindow;
}

class WaitingSpinnerWidget;

class AdvancedSearchResults : public QMainWindow {
private:
  Q_OBJECT

  Ui::AdvancedSearchResultsWindow* ui;
  WaitingSpinnerWidget* m_spinner;

public:
  explicit AdvancedSearchResults(QWidget* parent = 0);
  ~AdvancedSearchResults();

public slots:
  void setResults(const QList<QByteArray>& results);

protected:
  void closeEvent(QCloseEvent* event);

signals:
  void new_tab_with_cursor_request(const hawk::Path& p);
};

#endif // SEARCHRESULTS_H
