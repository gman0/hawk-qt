#include "focuslineedit.h"
#include <QFocusEvent>

void FocusLineEdit::focusInEvent(QFocusEvent* event)
{
  QLineEdit::focusInEvent(event);
  emit focusAcquired();
}

void FocusLineEdit::focusOutEvent(QFocusEvent* event)
{
  QLineEdit::focusOutEvent(event);
  emit focusLost();
}
