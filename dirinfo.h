#ifndef DIRINFO_H
#define DIRINFO_H

#include <hawk/Filesystem.h>

struct DirInfo {
  uintmax_t items;
  uintmax_t freespace;
  uintmax_t capacity;
};

#endif // DIRINFO_H
