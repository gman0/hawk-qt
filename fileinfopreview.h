#ifndef FILEINFOPREVIEW_H
#define FILEINFOPREVIEW_H

#include <QWidget>
#include <hawk/Path.h>

namespace Ui {
class FileInfoPreview;
}

class FileInfoPreview : public QWidget {
private:
  Q_OBJECT

public:
  explicit FileInfoPreview(QWidget* parent = nullptr);
  ~FileInfoPreview();

  void set_path(const hawk::Path& p);

private:
  Ui::FileInfoPreview* ui;
};

#endif // FILEINFOPREVIEW_H
