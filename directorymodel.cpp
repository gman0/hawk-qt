#include "directorymodel.h"
#include "utils.h"
#include <QDebug>
#include <QFont>
#include <QIcon>
#include <QMimeData>
#include <QUrl>
#include <algorithm>

#include <iostream>

using namespace std;

void DirectoryModel::updateDirectory(Data data)
{
  beginResetModel();
  m_data = std::move(data);
  endResetModel();
}

void DirectoryModel::scannedAt(int from, int to)
{
  emit dataChanged(createIndex(from, 0), createIndex(to, 0),
                   {Qt::DecorationRole});
  emit dataChanged(createIndex(from, 1), createIndex(to, 1));
}

QVariant DirectoryModel::loadThumbnail(int row) const
{
  Thumbnail_cache_data* data = m_data.thumbs.get();
  if (!data)
    return QVariant();

  lock_guard<mutex> lk{data->m};
  auto it = data->map.find(row);
  if (it != data->map.end())
    return QVariant(it->second);

  return QVariant();
}

QVariant DirectoryModel::nameColumn(hawk::Directory::Sorted sr, int idx,
                                    int role) const
{
  if (role == Qt::DecorationRole) {
    if (hawk::is_directory(sr.type(idx)))
      return m_icons.icon(QFileIconProvider::Folder);
    else {
      QVariant thumb = loadThumbnail(sr.index(idx).value());
      if (!thumb.isNull())
        return thumb;

      return m_icons.icon(QFileIconProvider::File);
    }
  }

  if (role == Qt::DisplayRole)
    return sr.name(idx);

  if (role == Qt::FontRole) {
    QFont font;
    hawk::Stat st;

    if (hawk::is_symlink(sr.type(idx))) {
      font.setItalic(true);

      if (hawk::is_symlink_broken(sr.type(idx)))
        return font;

      int err;
      st = hawk::status(m_data.dir.path() / sr.name(idx), err);
      if (err != 0)
        return font;
    }
    else {
      lock_guard<mutex> lk{m_data.stats->m};
      st = m_data.stats->map[sr.index(idx).value()];
    }

    if (hawk::is_regular_file(st) && (st.st_mode & S_IXUSR))
      font.setBold(true);

    return font;
  }

  if (role == Qt::ForegroundRole) {
    QColor c;
    if (hawk::is_symlink_broken(sr.type(idx)))
      c = Qt::red;

    return c;
  }

  return QVariant();
}

QVariant DirectoryModel::data(const QModelIndex& index, int role) const
{
  if (!m_data.dir.valid() || index.row() >= static_cast<int>(m_data.dir.size()))
    return QVariant();

  auto sr = m_data.dir.sorted_access();

  if (index.column() == 0)
    return nameColumn(sr, index.row(), role);
  else if (index.column() == 1) {
    if (role == Qt::DisplayRole) {
      if (hawk::is_regular_file(sr.type(index.row()))) {
        if (m_data.stats) {
          char size_str[20];

          lock_guard<mutex> lk{m_data.stats->m};
          return QString(human_readable_filesize(
              m_data.stats->map[sr.index(index.row()).value()].st_size,
              size_str));
        }
      }
    }
  }

  //  if (role == Qt::ForegroundRole)
  //    return QColor(Qt::lightGray);

  return QVariant();
}

QVariant DirectoryModel::headerData(int section, Qt::Orientation orientation,
                                    int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant();

  if (orientation == Qt::Horizontal) {
    switch (section) {
    case 0:
      return "Name";
    case 1:
      return "Size";
    }
  }

  return QVariant();
}

QModelIndex DirectoryModel::index(int row, int column, const QModelIndex&) const
{
  return createIndex(row, column);
}

QModelIndex DirectoryModel::parent(const QModelIndex&) const
{
  return QModelIndex();
}

int DirectoryModel::rowCount(const QModelIndex&) const
{
  if (m_data.dir.valid())
    return m_data.dir.size();

  return 0;
}

int DirectoryModel::columnCount(const QModelIndex&) const { return 2; }

Qt::ItemFlags DirectoryModel::flags(const QModelIndex& index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index) | Qt::ItemNeverHasChildren;

  if (index.isValid()) {
    if (index.column() == 0)
      return f | Qt::ItemIsEditable | Qt::ItemIsDragEnabled |
             Qt::ItemIsDropEnabled;
  }

  if (index.column() == 0) {
    if (index.isValid()) {
      return f | Qt::ItemIsEditable | Qt::ItemIsDragEnabled |
             Qt::ItemIsDropEnabled;
    }

    return f;
  }

  return f | Qt::ItemIsDropEnabled;
}

constexpr const char* uri_mime = "text/uri-list";

QStringList DirectoryModel::mimeTypes() const
{
  return QStringList() << uri_mime;
}

QMimeData* DirectoryModel::mimeData(const QModelIndexList& indexes) const
{
  QMimeData* mimeData = new QMimeData();
  QList<QUrl> urls;

  for (const QModelIndex& idx : indexes) {
    if (idx.isValid()) {
      urls << QUrl(QString("file://%1/%2")
                       .arg(m_data.dir.path().c_str())
                       .arg(data(idx, Qt::DisplayRole).toString()));
    }
  }

  mimeData->setUrls(urls);
  return mimeData;
}

bool DirectoryModel::canDropMimeData(const QMimeData* data, Qt::DropAction, int,
                                     int column, const QModelIndex&) const
{
  if (!data->hasUrls())
    return false;

  if (column > 0)
    return false;

  return true;
}

Qt::DropActions DirectoryModel::supportedDropActions() const
{
  return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}

/*
bool DirectoryModel::dropMimeData(const QMimeData* data, Qt::DropAction action,
                                  int row, int column,
                                  const QModelIndex& parent)
{
  if (!canDropMimeData(data, action, row, column, parent))
    return false;

  if (action == Qt::IgnoreAction)
    return true;

  QByteArray encoded_data = data->data(uri_mime);
  QDataStream stream{&encoded_data, QIODevice::ReadOnly};

  while (!stream.atEnd()) {
    QString text;
    stream >> text;
    qDebug() << text;
  }

  return true;
}
*/
