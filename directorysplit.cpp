#include "directorysplit.h"
#include "columngroup.h"
#include "commandbar.h"
#include "errorhandling.h"
#include "filterbar.h"
#include "pathbar.h"
#include "pathbaredit.h"
#include "searchbar.h"
#include <QVBoxLayout>

DirectorySplit::DirectorySplit(QWidget* parent) : QWidget{parent} { init(); }

DirectorySplit::DirectorySplit(const hawk::Path& dirpath, const Cursor& c,
                               QWidget* parent)
    : QWidget{parent}
{
  init();
  m_cg->cursors().store(dirpath, c);
}

void DirectorySplit::set_cursors(const CursorCache& cursors)
{
  m_cg->set_cursors(cursors);
}

hawk::Path DirectorySplit::dirpath() { return m_cg->dirpath(); }

void DirectorySplit::reload_path() { m_cg->reload_path(); }

void DirectorySplit::set_path(const hawk::Path& dirpath)
{
  m_cg->set_path(dirpath, 0);
}

ColumnGroup* DirectorySplit::columngroup() const { return m_cg; }

void DirectorySplit::show_pathbar_edit()
{
  m_cmdbar->pathbar_edit()->setText(m_cg->dirpath().c_str());
  m_cmdbar->show_pathbar_edit();
}

void DirectorySplit::show_searchbar() { m_cmdbar->show_searchbar(); }

void DirectorySplit::show_filterbar() { m_cmdbar->show_filterbar(); }

void DirectorySplit::show_advanced_search()
{
  m_cmdbar->show_advanced_search();
}

void DirectorySplit::init()
{
  init_cg();
  init_cmdbar();

  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->addWidget(m_cg);
  layout->addWidget(m_cmdbar);
  setLayout(layout);

  connect(m_cg, &ColumnGroup::path_changed,
          [this](const hawk::Path& p) { emit path_changed(p); });
}

void DirectorySplit::init_cg()
{
  m_cg = new ColumnGroup{1, this};

  connect(m_cg, &ColumnGroup::error_occured, this,
          [this](hawk::Filesystem_error e) {
            std::cerr << "Filesystem error: " << e.what() << std::endl;
            display_filesystem_error(e);
            m_cg->set_path(m_cg->dirpath().parent_path(), 0);
          },
          Qt::QueuedConnection);
  connect(m_cg, &ColumnGroup::new_tab_request,
          [this](const hawk::Path& dirpath) { emit new_tab_request(dirpath); });
  connect(m_cg, &ColumnGroup::io_request,
          [&](FileOperation op, const QList<QUrl>& srcs,
              const hawk::Path& dst) { emit io_request(op, srcs, dst); });
}

void DirectorySplit::init_cmdbar()
{
  m_cmdbar = new CommandBar{m_cg, this};

  connect(m_cmdbar->pathbar(), &PathBar::clicked,
          [this](const hawk::Path& dirpath) {
            if (dirpath == this->dirpath())
              m_cg->reload_path();
            else
              m_cg->set_path(dirpath, 0);
          });
  connect(m_cg, &ColumnGroup::path_changed, [this](const hawk::Path& dirpath) {
    m_cmdbar->pathbar()->set_path(dirpath);
  });
  connect(m_cmdbar->pathbar_edit(), &PathBarEdit::returnPressed, [this] {
    m_cg->set_path(m_cmdbar->pathbar_edit()->text().toStdString(), 0);
    m_cmdbar->show_pathbar();
  });
  connect(m_cmdbar->pathbar(), &PathBar::doubleClicked, [this] {
    m_cmdbar->pathbar_edit()->set_path(m_cmdbar->pathbar()->path());
    m_cmdbar->show_pathbar_edit();
  });
  connect(m_cmdbar->pathbar_edit(), &PathBarEdit::focusLost,
          [this] { m_cmdbar->show_pathbar(); });

  connect(m_cmdbar->filterbar(), &FilterBar::filterChanged,
          [this](const QString& str) {
            m_cg->filter_by_name(str);
            m_cmdbar->show_pathbar();
          });
  connect(m_cmdbar->filterbar(), &FilterBar::focusLost,
          [this] { m_cmdbar->show_pathbar(); });

  connect(m_cmdbar->searchbar(), &SearchBar::advancedSearch,
          [this](const hawk::Path& base, const AdvancedSearch::Criteria& cr) {
            m_cmdbar->show_pathbar();
            emit advanced_search(base, cr);
          });
  connect(m_cmdbar->searchbar(), &SearchBar::focusLost,
          [this] { m_cmdbar->show_pathbar(); });
}
