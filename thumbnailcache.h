#ifndef THUMBNAILCACHE_H
#define THUMBNAILCACHE_H

#include <QImage>
#include <hawk/Cache.h>
#include <hawk/Region.h>
#include <map>
#include <mutex>

struct Thumbnail_cache_data {
  hawk::Region_list rs;
  std::map<uint64_t, QImage> map;
  std::mutex m;

  void clear()
  {
    rs.clear();
    map.clear();
  }
};

using Thumbnail_cache = hawk::Simple_cache<Thumbnail_cache_data>;

#endif // THUMBNAILCACHE_H
