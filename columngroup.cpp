#include "columngroup.h"
#include "fileoperation.h"
#include "filter.h"
#include "utils.h"
#include <QDebug>
#include <QDesktopServices>
#include <chrono>
#include <hawk/watchdog/Inotify_monitor.h>

using namespace std;

ColumnGroup::ColumnGroup(int columns, QWidget* parent)
    : QSplitter{parent},
      m_cache([this](const hawk::Path& p) { m_wd.remove_path(p); }),
      m_wd(make_unique<hawk::Inotify_monitor>(chrono::milliseconds{50}),
           [this](auto e, const auto& dir) { this->on_wd_event(e, dir); }),
      m_active_filters({Filter_hidden()}), m_filters_id{0}
{
  for (int i = 0; i < columns; i++) {
    DirectoryWidget* w =
        new DirectoryWidget(m_wd, m_cache, m_cursors, nullptr, this);
    m_parents.push_back(w);

    connect(w, &DirectoryWidget::io_request,
            [&](FileOperation op, const QList<QUrl>& srcs,
                const hawk::Path& dst) { emit io_request(op, srcs, dst); });
    connect(w, &DirectoryWidget::item_clicked, this, &ColumnGroup::side_click);
    connect(w, &DirectoryWidget::error_occured,
            [this](hawk::Filesystem_error e) { emit error_occured(e); });

    w->set_filters(m_active_filters, m_filters_id);

    addWidget(w);
  }

  QList<int> widths;
  for (int i = 0; i < columns; i++) {
    widths << 100;
  }
  setSizes(widths);

  m_root = new RootDirectoryWidget(m_wd, m_cache, m_cursors, this);
  m_preview = new PreviewWidget(m_wd, m_cache, m_cursors, this);

  m_root->set_filters(m_active_filters, m_filters_id);
  m_preview->directory()->set_filters(m_active_filters, m_filters_id);

  connect(m_preview->directory(), &DirectoryWidget::item_clicked, this,
          &ColumnGroup::side_click);
  connect(m_preview, &PreviewWidget::error_occured,
          [this](hawk::Filesystem_error e) { emit error_occured(e); });

  addWidget(m_root);
  addWidget(m_preview);

  setCollapsible(count() - 2, false);
  setStretchFactor(count() - 2, 5);

  connect(m_root, &RootDirectoryWidget::update_preview, m_preview,
          &PreviewWidget::set_path);
  connect(m_root, &RootDirectoryWidget::activated,
          [this](const QModelIndex& index) {
            hawk::Path p = m_dirpath /
                           static_cast<const DirectoryModel*>(index.model())
                               ->data()
                               .dir.sorted_access()
                               .name(index.row());

            int err;
            hawk::Stat st = hawk::status(p, err);

            if (err == 0) {
              if (hawk::is_directory(st))
                set_path(p, 0);
              else
                open_file(p);
            }
          });
  connect(m_root, &RootDirectoryWidget::error_occured,
          [this](hawk::Filesystem_error e) { emit error_occured(e); });
  connect(m_root, &RootDirectoryWidget::new_tab_request,
          [this](const hawk::Path& dirpath) { emit new_tab_request(dirpath); });
  connect(m_root, &RootDirectoryWidget::io_request,
          [this](FileOperation op, const QList<QUrl>& srcs,
                 const hawk::Path& dst) { emit io_request(op, srcs, dst); });
  connect(m_root, &RootDirectoryWidget::update_dirinfo,
          [this](DirInfo i) { emit update_dirinfo(i); });
}

hawk::Path ColumnGroup::dirpath()
{
  lock_guard<recursive_mutex> lk{m_mtx};
  return m_dirpath;
}

void ColumnGroup::set_path(hawk::Path dirpath, unsigned page)
{
  if (dirpath.empty())
    return;

  if (!hawk::exists(dirpath) || !hawk::is_directory(dirpath))
    return;

  lock_guard<recursive_mutex> lk{m_mtx};

  m_dirpath = dirpath;
  m_page = page;

  m_cursors.store(dirpath);

  // In case the preview is still loading the directory
  // we're trying to set_path() into.
  m_preview->directory()->release();

  if (dirpath == "/")
    widget(0)->setDisabled(true);
  else
    widget(0)->setEnabled(true);

  m_root->set_path(dirpath, page);
  for (auto it = m_parents.rbegin(); it != m_parents.rend(); ++it) {
    dirpath.set_parent_path();
    (*it)->set_path(dirpath, 0);
  }

  emit path_changed(m_dirpath);
}

void ColumnGroup::reload_path()
{
  lock_guard<recursive_mutex> lk{m_mtx};

  m_preview->directory()->release();

  m_root->reload_path();
  for (auto it = m_parents.rbegin(); it != m_parents.rend(); ++it)
    (*it)->reload_path();
}

void ColumnGroup::filter_by_name(const QString& str)
{
  if (str.isEmpty())
    update_filters({Filter_hidden()});
  else {
    if (str.startsWith('/') && str.endsWith('/') && str.length() > 2) {
      update_filters({Filter_hidden(), Filter_files_by_name_regex(
                                           str.mid(1, str.length() - 2))});
    }
    else
      update_filters({Filter_hidden(), Filter_files_by_name(str)});
  }
}

DirInfo ColumnGroup::dirinfo() const { return m_root->dirinfo(); }

void ColumnGroup::side_click(const hawk::Path& p)
{
  hawk::Path parent = p.parent_path();
  if (parent == dirpath())
    set_path(p, 0);
  else {
    m_cursors.store(p);
    set_path(parent, 0);
  }
}

void ColumnGroup::on_wd_event(hawk::Monitor::Event,
                              const hawk::Path& dirpath) noexcept
{
  lock_guard<recursive_mutex> lk{m_mtx};

  // Change occurred in root
  if (m_root->path() == dirpath) {
    m_root->reload_path();
    return;
  }

  // Change occurred in one of the parents
  auto found =
      std::find_if(m_parents.begin(), m_parents.end(),
                   [&](const auto& wptr) { return wptr->path() == dirpath; });
  if (found != m_parents.end()) {
    (*found)->reload_path();
    return;
  }

  // Change occurred in the preview
  if (m_preview->directory()->path() == dirpath) {
    m_preview->directory()->reload_path();
    return;
  }

  // Otherwise just discard all caches
  m_cache.dcache.discard(dirpath);
  m_cache.scache.discard(dirpath);
  m_cache.tcache.discard(dirpath);
}

void ColumnGroup::release()
{
  m_preview->directory()->release();

  for (DirectoryWidget* p : m_parents)
    p->release();

  m_root->release();

  m_cache.dcache.discard_all();
  m_cache.scache.discard_all();
  m_cache.tcache.discard_all();
}

void ColumnGroup::update_filters(hawk::Directory_data::Filters&& fs)
{
  {
    lock_guard<recursive_mutex> lk{m_mtx};

    m_active_filters = std::move(fs);
    ++m_filters_id;

    release();
  }

  m_root->set_filters(m_active_filters, m_filters_id);
  for (DirectoryWidget* p : m_parents)
    p->set_filters(m_active_filters, m_filters_id);

  m_preview->directory()->set_filters(m_active_filters, m_filters_id);

  set_path(m_dirpath, m_page);
}
