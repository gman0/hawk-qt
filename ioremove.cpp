#include "ioremove.h"
#include "utils.h"

IORemove::IORemove(const QList<QUrl>& srcs, const hawk::Path&, QObject* parent)
    : QObject(parent), hawk::IO_task_remove{to_paths_vector(srcs)}
{
}

void IORemove::on_status_change(hawk::IO_task::Status st) noexcept
{
  emit status_changed(st);
}

void IORemove::on_task_progress(const Total_progress& tp) noexcept
{
  emit task_progressed(tp);
}

void IORemove::on_file_progress(const hawk::IO_task::File_progress& fp) noexcept
{
  emit file_progressed(fp);
}

void IORemove::handle_error(const hawk::Filesystem_error& e,
                            hawk::IO_task::Target& t) noexcept
{
  emit error_occured(e, &t, this);
}

void IORemove::handle_prepare_error(const hawk::Filesystem_error&) noexcept {}
