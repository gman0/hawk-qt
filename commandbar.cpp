#include "commandbar.h"
#include "filterbar.h"
#include "pathbar.h"
#include "pathbaredit.h"
#include "pathcompletionmodel.h"
#include "searchbar.h"
#include <QComboBox>
#include <QCompleter>
#include <QStackedLayout>

CommandBar::CommandBar(ColumnGroup* cg, QWidget* parent) : QWidget(parent)
{
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

  QWidget* root = new QWidget(this);
  root->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

  QHBoxLayout* rootLayout = new QHBoxLayout();
  rootLayout->setContentsMargins(0, 0, 0, 0);

  m_cb = new QComboBox();
  m_cb->addItem("Path");
  m_cb->addItem("Filter");
  m_cb->addItem("Search");

  rootLayout->addWidget(m_cb);
  rootLayout->addWidget(root);

  m_pathbar = new PathBar(root);
  m_pathbar_edit = new PathBarEdit(root);

  QCompleter* c = new QCompleter(root);
  PathCompletionModel* m = new PathCompletionModel(root);
  m->set_path("/");
  c->setModel(m);
  m_pathbar_edit->setCompleter(c);

  m_filterbar = new FilterBar(root);
  m_searchbar = new SearchBar(cg, root);

  m_layout = new QStackedLayout();
  m_layout->addWidget(m_pathbar);
  m_layout->addWidget(m_pathbar_edit);
  m_layout->addWidget(m_filterbar);
  m_layout->addWidget(m_searchbar);

  root->setLayout(m_layout);
  setLayout(rootLayout);

  connect(m_cb, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
          [this](int index) {
            switch (index) {
            case 0:
              show_pathbar();
              break;
            case 1:
              show_filterbar();
              break;
            case 2:
              show_searchbar();
              break;
            }
          });
}

void CommandBar::show_pathbar()
{
  m_layout->setCurrentWidget(m_pathbar);
  m_cb->setCurrentIndex(0);
}

void CommandBar::show_pathbar_edit()
{
  m_layout->setCurrentWidget(m_pathbar_edit);
  m_cb->setCurrentIndex(0);
}

void CommandBar::show_filterbar()
{
  m_layout->setCurrentWidget(m_filterbar);
  m_cb->setCurrentIndex(1);
}

void CommandBar::show_searchbar()
{
  m_layout->setCurrentWidget(m_searchbar);
  m_cb->setCurrentIndex(2);
}

void CommandBar::show_advanced_search() { m_searchbar->show_advanced_search(); }
