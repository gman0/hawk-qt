#ifndef COMMANDBAR_H
#define COMMANDBAR_H

#include <QWidget>

class PathBar;
class PathBarEdit;
class FilterBar;
class SearchBar;
class ColumnGroup;
class QStackedLayout;
class QComboBox;

class CommandBar : public QWidget {
private:
  Q_OBJECT

  PathBar* m_pathbar;
  PathBarEdit* m_pathbar_edit;
  FilterBar* m_filterbar;
  SearchBar* m_searchbar;

  QComboBox* m_cb;
  QStackedLayout* m_layout;

public:
  CommandBar(ColumnGroup* cg, QWidget* parent = nullptr);

  PathBar* pathbar() const { return m_pathbar; }
  PathBarEdit* pathbar_edit() const { return m_pathbar_edit; }
  FilterBar* filterbar() const { return m_filterbar; }
  SearchBar* searchbar() const { return m_searchbar; }

public slots:
  void show_pathbar();
  void show_pathbar_edit();
  void show_filterbar();
  void show_searchbar();
  void show_advanced_search();
};

#endif // COMMANDBAR_H
