import QtQuick 2.0
import hawk.process 1.0

Item {
    Process {
        id: proc
    }

    function openTextEditor(filepath) {
        // Runs system's default text editor (editor) in system's default terminal (x-terminal-emulator)
        proc.start('x-terminal-emulator', ['-e', 'editor', filepath])
        proc.waitForFinished()

        return proc.exitCode()
    }
}
