#include "data.h"
#include "mainwindow.h"
#include "process.h"
#include "sortcriterium.h"
#include <QAbstractItemModel>
#include <QApplication>
#include <QSettings>
#include <QTextBlock>
#include <QtQml/QtQml>
#include <hawk/Path.h>
#include <hawk/io/IO_task.h>

int main(int argc, char* argv[])
{
  qRegisterMetaType<Data>("Data");
  qRegisterMetaType<hawk::Path>("hawk::Path");
  qRegisterMetaType<hawk::IO_task::Status>("hawk::IO_task::Status");
  qRegisterMetaType<QTextBlock>("QTextBlock");
  qRegisterMetaType<QVector<int>>("QVector<int>");
  qRegisterMetaType<hawk::IO_task::File_progress>(
      "hawk::IO_task::File_progress");
  qRegisterMetaType<hawk::IO_task::Total_progress>(
      "hawk::IO_task::Total_progress");
  qRegisterMetaType<hawk::Filesystem_error>("hawk::Filesystem_error");
  qRegisterMetaType<hawk::IO_task::Target>("hawk::IO_task::Target");

  qmlRegisterType<Process>("hawk.process", 1, 0, "Process");

  QApplication a(argc, argv);
  QCoreApplication::setOrganizationName("codefreax");
  QCoreApplication::setOrganizationDomain("codefreax.org");
  QCoreApplication::setApplicationName("hawk");

  QSettings settings;
  settings.setValue("sort", static_cast<int>(SortCriterium::natural));

  MainWindow w;
  w.show();

  return a.exec();
}
