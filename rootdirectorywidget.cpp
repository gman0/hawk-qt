#include "rootdirectorywidget.h"
#include "bulkrenamewindow.h"
#include "directoryitemdelegate.h"
#include "filter.h"
#include "newdirectorywindow.h"
#include "removeconfirmwindow.h"
#include "sort.h"
#include "sortcriterium.h"
#include <QClipboard>
#include <QContextMenuEvent>
#include <QDebug>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QGuiApplication>
#include <QHeaderView>
#include <QMenu>
#include <QMimeData>
#include <QScrollBar>
#include <QSettings>
#include <QUrl>

using namespace std;

RootDirectoryWidget::RootDirectoryWidget(hawk::Watchdog& w, Cache& c,
                                         CursorCache& cc, QWidget* parent)
    : QTreeView{parent}, data{w, c, cc, nullptr,
                              [this](exception_ptr eptr) {
                                try {
                                  rethrow_exception(eptr);
                                }
                                catch (const hawk::Filesystem_error& e) {
                                  emit error_occured(e);
                                }
                              },
                              this}
{
  setMinimumSize(500, 500);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

  setAllColumnsShowFocus(true);
  setAlternatingRowColors(true);
  setRootIsDecorated(false);
  setUniformRowHeights(true);
  setItemsExpandable(false);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  setSelectionBehavior(QAbstractItemView::SelectRows);

  setItemDelegate(new DirectoryItemDelegate());
  setModel(&data.model);
  setSelectionModel(&data.selmodel);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  //  setAcceptDrops(true);
  setDragEnabled(true);
  setDragDropMode(QAbstractItemView::DragDrop);
  viewport()->setAcceptDrops(true);
  setDropIndicatorShown(true);

  setEditTriggers(EditTrigger::NoEditTriggers);

  connect(&data.selmodel, &QItemSelectionModel::currentChanged, this,
          [this](const QModelIndex& current, const QModelIndex&) {
            {
              lock_guard<mutex> lk{data.m};

              auto sr = data.model.data().dir.sorted_access();
              if (sr.size() == 0) {
                emit update_preview(hawk::Path());
                return;
              }

              data.cursors.store(data.dirpath,
                                 Cursor{current.row(), sr.inode(current.row()),
                                        sr.name(current.row())});

              emit update_preview(data.dirpath / sr.name(current.row()));
            }

            int step = verticalScrollBar()->pageStep();
            scan_at(max(0, current.row() - step),
                    min(data.model.rowCount(), current.row() + step));
          });

  QHeaderView* h = header();
  h->setSortIndicatorShown(true);
  h->setSortIndicator(0, Qt::AscendingOrder);
  h->setStretchLastSection(false);
  h->setSectionResizeMode(0, QHeaderView::Stretch);
  h->setSectionsClickable(true);

  connect(h, &QHeaderView::sortIndicatorChanged,
          [this](int idx, Qt::SortOrder order) {
            QSettings s;
            SortCriterium cr;
            if (idx == 0) {
              cr = (order == Qt::AscendingOrder) ? SortCriterium::natural
                                                 : SortCriterium::natural_rev;
            }
            else if (idx == 1) {
              cr = (order == Qt::AscendingOrder) ? SortCriterium::size
                                                 : SortCriterium::size_rev;
            }

            s.setValue("sort", static_cast<int>(cr));
            reload_path();
          });
}

void RootDirectoryWidget::set_path(const hawk::Path& dirpath, unsigned page)
{
  lock_guard<mutex> lk{data.m};

  data.model.updateDirectory({});

  data.dirpath = dirpath;
  data.page = page;

  if (dirpath.empty())
    return;

  set_path_tasks(dirpath, page, TaskPriorty::setPath_load_dcache);
}

void RootDirectoryWidget::reload_path()
{
  lock_guard<mutex> lk{data.m};
  data.model.updateDirectory({});

  data.ts->run_noint_blocking(make_unique<Discard_dcache_task>(
      TaskPriorty::discard, data.cache, data.dirpath));
  data.ts->run_noint_blocking(make_unique<Discard_scache_task>(
      TaskPriorty::discard, data.cache, data.dirpath));
  data.ts->run_noint_blocking(make_unique<Discard_tcache_task>(
      TaskPriorty::discard, data.cache, data.dirpath));

  if (data.dirpath.empty())
    return;

  set_path_tasks(data.dirpath, data.page, TaskPriorty::reloadPath_load_dcache);
}

void RootDirectoryWidget::set_cursor(int row)
{
  QModelIndex idx = data.model.index(row, 0);
  setCurrentIndex(idx);
  scrollTo(idx, QAbstractItemView::PositionAtCenter);
}

void RootDirectoryWidget::scan_at(int from, int to)
{
  auto load_scache = make_unique<Load_scache_task>(
      TaskPriorty::load_scache, data.cache, data.dirpath, data.page,
      [from, to] {
        return hawk::Region{(uint64_t)from, (uint64_t)to};
      });
  auto load_tcache = make_unique<Load_tcache_task>(
      TaskPriorty::load_scache, data.cache, data.dirpath, data.page,
      [from, to] {
        return hawk::Region{(uint64_t)from, (uint64_t)to};
      });
  auto unload_scache = make_unique<Unload_scache_task>(
      TaskPriorty::load_scache, (from + to) / 2, data.model.data().dir,
      data.model.data().stats);
  auto unload_tcache = make_unique<Unload_tcache_task>(
      TaskPriorty::load_scache, (from + to) / 2, data.model.data().dir,
      data.model.data().thumbs);
  auto update_model =
      make_unique<Scanned_dirmodel_task>(TaskPriorty::load_scache, from, to);
  auto update_model2 =
      make_unique<Scanned_dirmodel_task>(TaskPriorty::load_scache, from, to);

  connect(update_model.get(), &Scanned_dirmodel_task::scannedItems, &data.model,
          &DirectoryModel::scannedAt);
  connect(update_model2.get(), &Scanned_dirmodel_task::scannedItems,
          &data.model, &DirectoryModel::scannedAt);

  lock_guard<mutex> lk{data.m};
  data.ts->run(std::move(load_scache), std::move(unload_scache),
               std::move(update_model), std::move(load_tcache),
               std::move(unload_tcache), std::move(update_model2));
}

void RootDirectoryWidget::set_path_tasks(const hawk::Path& dirpath,
                                         unsigned page,
                                         TaskPriorty set_path_priority)
{
  auto load_dcache = make_unique<Load_dcache_task>(
      set_path_priority, data.cache, data.wd, data.filters, dirpath, page);
  auto sort_dir =
      create_sort_task(TaskPriorty::sort, data.cache, dirpath, page);
  auto update_model = make_unique<Update_dirmodel_task>(
      set_path_priority, data.cache, data.model, dirpath, page);
  auto set_cursor = make_unique<Set_cursor_task>(
      TaskPriorty::set_cursor, data.cache, data.cursors, dirpath, page);

  connect(update_model.get(), &Update_dirmodel_task::modelUpdated, &data.model,
          &DirectoryModel::updateDirectory);
  connect(update_model.get(), &Update_dirmodel_task::viewReady, this,
          &RootDirectoryWidget::ready);
  connect(set_cursor.get(), &Set_cursor_task::cursorSet, this,
          &RootDirectoryWidget::set_cursor);

  disconnect(verticalScrollBar(), nullptr, nullptr, nullptr);
  verticalScrollBar()->setValue(0);
  busy();
  connect(
      verticalScrollBar(), &QScrollBar::valueChanged, this, [this](int value) {
        int step = verticalScrollBar()->pageStep();
        scan_at(max(0, value - step), min(data.model.rowCount(), value + step));
      });
  // TODO : rangeChanged

  data.ts->run(std::move(load_dcache), std::move(sort_dir),
               std::move(update_model), std::move(set_cursor));
}

void RootDirectoryWidget::set_filters(const hawk::Directory_data::Filters& fs,
                                      size_t id)
{
  lock_guard<mutex> lk{data.m};
  data.filters = fs;
  data.filters_id = id;
}

void RootDirectoryWidget::release() { data.release(); }

void RootDirectoryWidget::busy() { data.wspinner.start(); }

void RootDirectoryWidget::ready()
{
  data.wspinner.stop();
  emit update_dirinfo(dirinfo());
}

DirInfo RootDirectoryWidget::dirinfo() const
{
  DirInfo i;
  int err;
  const hawk::Space_info si = hawk::space(path(), err);

  i.items = model()->rowCount();
  i.freespace = si.available;
  i.capacity = si.capacity;

  return i;
}

hawk::Path RootDirectoryWidget::path() const
{
  lock_guard<mutex> lk{data.m};
  return data.dirpath;
}

unsigned RootDirectoryWidget::page() const
{
  lock_guard<mutex> lk{data.m};
  return data.page;
}

void RootDirectoryWidget::contextMenuEvent(QContextMenuEvent* event)
{
  QModelIndex idx = indexAt(event->pos());
  if (!idx.isValid()) {
    QMenu menu;
    menu.addAction("Open in new tab",
                   [&] { emit new_tab_request(data.dirpath); });
    menu.addAction("Open terminal here");
    menu.addAction("Properties");
    menu.addSeparator();
    menu.addAction("New directory", [&] {
      NewDirectoryWindow* w = new NewDirectoryWindow{path(), this};
      w->show();
    });

    menu.exec(mapToGlobal(event->pos()));
    QTreeView::contextMenuEvent(event);

    return;
  }

  const hawk::Directory& dir = data.model.data().dir;
  auto sr = dir.sorted_access();
  QModelIndexList sel = selectionModel()->selectedRows();
  QMenu menu;

  if (sel.size() == 1) {
    menu.addAction("Open");
    if (hawk::is_directory(sr.type(idx.row()))) {
      menu.addAction("Open in new tab", [&] {
        emit new_tab_request(data.dirpath / sr.name(idx.row()));
      });
    }

    menu.addSeparator();

    menu.addAction("Rename", [&] { edit(idx); });
    menu.addAction("Remove", [&] {
      RemoveConfirmWindow* w = new RemoveConfirmWindow{path(), sel, this};
      connect(w, &RemoveConfirmWindow::removeConfirmed,
              [this, sel] { remove_selection(sel); });

      w->show();
    });
    menu.addAction("Add to archive...");
    menu.addAction("New directory", [&] {
      NewDirectoryWindow* w = new NewDirectoryWindow{path(), this};
      w->show();
    });
    menu.addSeparator();
    menu.addAction("Properties");
  }
  else {
    menu.addAction("Bulk rename...", [&] {
      BulkRenameWindow* w = new BulkRenameWindow{path(), sel, this};
      w->show();
      w->create_bulkfile();
    });
    menu.addAction("Remove", [&] {
      RemoveConfirmWindow* w = new RemoveConfirmWindow{path(), sel, this};
      connect(w, &RemoveConfirmWindow::removeConfirmed,
              [this, sel] { remove_selection(sel); });

      w->show();
    });
    menu.addAction("Add to archive...");
    menu.addSeparator();
    menu.addAction("Properties");
  }

  menu.exec(mapToGlobal(event->pos()));
  QTreeView::contextMenuEvent(event);
}

void RootDirectoryWidget::remove_selection(const QModelIndexList& idxs)
{
  hawk::Path base = path();
  QList<QUrl> urls;

  for (const QModelIndex& idx : idxs) {
    urls << QString("file://%1")
                .arg((base / idx.data().toString().toStdString()).c_str());
  }

  emit io_request(FileOperation::rm, urls, hawk::Path());
}

void RootDirectoryWidget::show_file_operations(const QMimeData* data,
                                               QPoint pos)
{
  QList<QUrl> urls = data->urls();
  QMenu menu;
  hawk::Path dst = this->data.get_dirpath();

  for (const QUrl& url : urls) {
    qDebug() << "url" << url.toLocalFile();
  }

  menu.addAction("Copy here",
                 [&] { emit io_request(FileOperation::cp, urls, dst); });
  menu.addAction("Move here",
                 [&] { emit io_request(FileOperation::mv, urls, dst); });
  //  menu.addAction("Link here",
  //                 [&] { emit io_request(FileOperation::ln, urls, dst); });

  menu.exec(mapToGlobal(pos));
}

void RootDirectoryWidget::dropEvent(QDropEvent* event)
{
  if (event->source() == this)
    return;

  const QMimeData* data = event->mimeData();
  if (!data->hasUrls())
    show_file_operations(data, event->pos());
}

void RootDirectoryWidget::keyPressEvent(QKeyEvent* event)
{
  QTreeView::keyPressEvent(event);

  // Check for CTRL
  if ((event->modifiers() & Qt::ControlModifier) != Qt::ControlModifier)
    return;

  QClipboard* clipboard = QGuiApplication::clipboard();

  // Extract
  if (event->key() == Qt::Key_C) {
    QModelIndexList rows = selectionModel()->selectedRows();
    const hawk::Path base = path();
    QMimeData* data = new QMimeData();

    QList<QUrl> urls;

    for (const QModelIndex& idx : rows) {
      urls << QUrl::fromLocalFile(
          (base / idx.data().toString().toStdString()).c_str());
    }

    qDebug() << urls;

    data->setUrls(urls);
    clipboard->setMimeData(data);
  }

  // Insert
  if (event->key() == Qt::Key_V) {
    const QMimeData* data = clipboard->mimeData();
    if (data->hasUrls())
      show_file_operations(data, mapFromGlobal(QCursor::pos()));

    return;
  }
}
