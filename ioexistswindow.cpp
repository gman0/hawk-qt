#include "ioexistswindow.h"
#include "ui_ioexistswindow.h"

IOExistsWindow::IOExistsWindow(QString from, QString to, QWidget* parent)
    : QMainWindow(parent), ui(new Ui::IOExistsWindow)
{
  ui->setupUi(this);
  ui->fromLabel->setText(from);
  ui->toLabel->setText(to);

  connect(ui->cancelBtn, &QPushButton::clicked, [this] { emit cancel(); });
  connect(ui->overwriteBtn, &QPushButton::clicked,
          [this] { emit overwrite(ui->checkBox->isChecked()); });
  connect(ui->renameBtn, &QPushButton::clicked,
          [this] { emit rename(ui->checkBox->isChecked()); });
}

IOExistsWindow::~IOExistsWindow() { delete ui; }

void IOExistsWindow::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}
