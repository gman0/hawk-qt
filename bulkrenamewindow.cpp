#include "bulkrenamewindow.h"
#include "ui_bulkrenamewindow.h"
#include <QDebug>
#include <QLineEdit>
#include <QMessageBox>
#include <QQmlComponent>
#include <QQmlEngine>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <memory>
#include <unistd.h>

using namespace std;

namespace {

void show_error_dialog(const QString& str)
{
  QMessageBox mbox;
  mbox.setText(str);
  mbox.setIcon(QMessageBox::Warning);
  mbox.setStandardButtons(QMessageBox::Ok);
  mbox.exec();
}

} // unnamed-namespace

BulkRenameWindow::BulkRenameWindow(const hawk::Path& base,
                                   const QModelIndexList& idxs, QWidget* parent)
    : QMainWindow(parent), ui(new Ui::BulkRenameWindow), m_base{base},
      m_bulkfile{0}, m_preamble_offset{0}, m_bulkfile_timestamp{0}
{
  ui->setupUi(this);
  ui->baseLabel->setText(base.c_str());
  ui->treeWidget->setItemDelegate(new BulkRenameDelegate(this));

  connect(ui->editorBtn, &QPushButton::clicked, this,
          &BulkRenameWindow::open_editor);
  connect(ui->renameBtn, &QPushButton::clicked, this,
          &BulkRenameWindow::bulk_rename);

  QList<QTreeWidgetItem*> items;
  for (const QModelIndex& idx : idxs) {
    QString name = idx.data().toString();
    QStringList data;
    data << name << name;

    QTreeWidgetItem* item =
        new QTreeWidgetItem(static_cast<QTreeWidget*>(nullptr), data);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    items.append(item);
  }

  ui->treeWidget->insertTopLevelItems(0, items);
}

BulkRenameWindow::~BulkRenameWindow()
{
  delete ui;

  if (m_bulkfile) {
    ::close(m_bulkfile);
    unlink(m_bulkfile_path.c_str());
  }
}

void BulkRenameWindow::open_editor()
{
  QQmlEngine engine;
  QQmlComponent comp(&engine, ":/scripts/opentexteditor.qml");
  unique_ptr<QObject> comp_instance{comp.create()};

  QVariant ret;
  QVariant arg{m_bulkfile_path.c_str()};
  QMetaObject::invokeMethod(comp_instance.get(), "openTextEditor",
                            Q_RETURN_ARG(QVariant, ret), Q_ARG(QVariant, arg));

  if (ret.toInt() != 0) {
    show_error_dialog(
        "Couldn't open a text editor, please check scripts/opentexteditor.qml");
    close();
    return;
  }

  // The file was updated
  time_t new_timestamp = hawk::last_write_time(m_bulkfile_path);
  if (m_bulkfile_timestamp < new_timestamp) {
    if (update_model_from_bulkfile())
      m_bulkfile_timestamp = new_timestamp;
    else
      close();
  }
}

void BulkRenameWindow::bulk_rename()
{
  auto m = ui->treeWidget->model();
  const int sz = m->rowCount();
  for (int i = 0; i < sz; i++) {
    hawk::rename(m_base / m->index(i, 0).data().toString().toStdString(),
                 m->index(i, 1).data().toString().toStdString());
  }

  close();
}

bool BulkRenameWindow::update_model_from_bulkfile()
{
  ifstream in{m_bulkfile_path.string()};
  // Skip the first 3 lines
  in.seekg(m_preamble_offset);

  auto m = ui->treeWidget->model();
  int count = m->rowCount();
  int n = 0;
  string line;

  while (getline(in, line)) {
    if (n > count)
      break;

    m->setData(m->index(n, 1), line.c_str());
    n++;
  }

  if (n != m->rowCount()) {
    show_error_dialog("Wrong row count, aborting!");
    return false;
  }

  return true;
}

void BulkRenameWindow::update_bulkfile()
{
  discard_bulkfile();
  write_model_to_bulkfile(1);
}

void BulkRenameWindow::write_model_to_bulkfile(int col)
{
  m_preamble_offset = 0;

  m_preamble_offset += dprintf(m_bulkfile, "# %s\n", m_bulkfile_path.c_str());
  m_preamble_offset +=
      dprintf(m_bulkfile, "# with root path in %s\n", m_base.c_str());
  m_preamble_offset += dprintf(m_bulkfile, "# DO NOT EDIT THOSE 3 LINES\n");

  auto m = ui->treeWidget->model();
  const int sz = m->rowCount();
  for (int i = 0; i < sz; i++) {
    dprintf(m_bulkfile, "%s\n",
            m->index(i, col).data().toString().toStdString().c_str());
  }

  m_bulkfile_timestamp = hawk::last_write_time(m_bulkfile_path);
}

void BulkRenameWindow::discard_bulkfile()
{
  lseek64(m_bulkfile, 0, SEEK_SET);
  ftruncate64(m_bulkfile, 0);
}

void BulkRenameWindow::create_bulkfile()
{
  char fname[] = "/tmp/hawk-bulkrename-XXXXXX";
  m_bulkfile = mkstemp64(fname);

  if (m_bulkfile < 0)
    throw hawk::Filesystem_error{errno};

  m_bulkfile_path = fname;

  write_model_to_bulkfile(0);
}

void BulkRenameWindow::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}

QWidget* BulkRenameDelegate::createEditor(QWidget* parent,
                                          const QStyleOptionViewItem&,
                                          const QModelIndex& index) const
{
  if (index.column() == 1) {
    QLineEdit* editor = new QLineEdit(parent);
    editor->setFrame(false);

    return editor;
  }

  return nullptr;
}

void BulkRenameDelegate::setModelData(QWidget* editor,
                                      QAbstractItemModel* model,
                                      const QModelIndex& index) const
{
  QLineEdit* lineEdit = static_cast<QLineEdit*>(editor);
  if (lineEdit->text().isEmpty())
    return;

  QStyledItemDelegate::setModelData(editor, model, index);
  static_cast<BulkRenameWindow*>(parent())->update_bulkfile();
}
