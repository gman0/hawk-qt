#include "pathbarbutton.h"
#include <QCursor>

PathBarButton::PathBarButton(const hawk::Path& dirpath, QWidget* parent)
    : QLabel{parent}
{
  setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
  setCursor(Qt::PointingHandCursor);
  setStyleSheet("font-weight: bold");

  set_path(dirpath);
}

void PathBarButton::set_path(const hawk::Path& dirpath)
{
  m_dirpath = dirpath;

  if (dirpath == "/")
    setText("  /  ");
  else
    setText(dirpath.filename().c_str());
}
