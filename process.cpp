#include "process.h"

// TODO: run in jail

void Process::start(const QString& program, const QVariantList& args)
{
  QStringList arglist;

  for (const QVariant& arg : args)
    arglist << arg.toString();

  QProcess::start(program, arglist);
}

void Process::waitForFinished() { QProcess::waitForFinished(-1); }

QByteArray Process::readAll() { return QProcess::readAll(); }

QByteArray Process::readAllBlocking()
{
  waitForReadyRead(-1);
  return QProcess::readAll();
}

QByteArray Process::readTillEOF()
{
  QByteArray data;
  while (waitForReadyRead(-1))
    data.append(QProcess::readAll());

  return data;
}

int Process::exitCode() const { return QProcess::exitCode(); }
