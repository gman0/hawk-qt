#ifndef ADDRESSBARBUTTON_H
#define ADDRESSBARBUTTON_H

#include "elidedlabel.h"
#include <hawk/Path.h>

class PathBarButton : public QLabel {
private:
  Q_OBJECT

  hawk::Path m_dirpath;

public:
  PathBarButton(const hawk::Path& dirpath, QWidget* parent = nullptr);

  const hawk::Path& path() const { return m_dirpath; }

public slots:
  void set_path(const hawk::Path& dirpath);
};

#endif // ADDRESSBARBUTTON_H
