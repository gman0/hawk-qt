#ifndef DIRECTORYMODEL_H
#define DIRECTORYMODEL_H

#include "data.h"
#include <QAbstractItemModel>
#include <QFileIconProvider>

class DirectoryModel : public QAbstractItemModel {
private:
  Q_OBJECT

  Data m_data;
  QFileIconProvider m_icons;

public:
  DirectoryModel(QObject* parent = nullptr) : QAbstractItemModel{parent} {}

  Data& data() { return m_data; }
  const Data& data() const { return m_data; }

  QVariant data(const QModelIndex& index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation,
                      int role) const override;
  QModelIndex index(int row, int column,
                    const QModelIndex& parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex& child = QModelIndex()) const override;
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

  QStringList mimeTypes() const override;
  QMimeData* mimeData(const QModelIndexList& indexes) const override;

  bool canDropMimeData(const QMimeData* data, Qt::DropAction action, int row,
                       int column, const QModelIndex& parent) const override;
  Qt::DropActions supportedDropActions() const override;
  /*
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row,
                    int column, const QModelIndex& parent) override;
                    */

public slots:
  void updateDirectory(Data data);
  void scannedAt(int from, int to);

private:
  QVariant loadThumbnail(int row) const;
  QVariant nameColumn(hawk::Directory::Sorted st, int idx, int role) const;
};

#endif // DIRECTORYMODEL_H
