#include "iomove.h"
#include "utils.h"

IOMove::IOMove(const QList<QUrl>& srcs, const hawk::Path& dst, QObject* parent)
    : QObject(parent), hawk::IO_task_move{to_paths_vector(srcs), dst}
{
}

void IOMove::on_status_change(hawk::IO_task::Status st) noexcept
{
  emit status_changed(st);
}

void IOMove::on_task_progress(const hawk::IO_task::Total_progress& tp) noexcept
{
  emit task_progressed(tp);
}

void IOMove::on_file_progress(const hawk::IO_task::File_progress& fp) noexcept
{
  emit file_progressed(fp);
}

void IOMove::handle_error(const hawk::Filesystem_error& e,
                          hawk::IO_task::Target& t) noexcept
{
  emit error_occured(e, &t, this);
}

void IOMove::handle_prepare_error(const hawk::Filesystem_error&) noexcept {}
