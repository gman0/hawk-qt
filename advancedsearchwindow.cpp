#include "advancedsearchwindow.h"
#include "ui_advancedsearchwindow.h"
#include <QCloseEvent>

AdvancedSearchWindow::AdvancedSearchWindow(const hawk::Path& base,
                                           QWidget* parent)
    : QMainWindow(parent), ui(new Ui::AdvancedSearchWindow), m_base{base}
{
  ui->setupUi(this);
  ui->baseLabel->setText(base.c_str());

  QComboBox* type = ui->typeCombo;
  type->addItem("Any");
  type->addItem("Files only", QString("f"));
  type->addItem("Directories only", QString("d"));

  connect(ui->searchBtn, &QPushButton::clicked, [this] {
    emit search_for(m_base, generate_criteria());
    close();
  });
}

AdvancedSearchWindow::~AdvancedSearchWindow() { delete ui; }

void AdvancedSearchWindow::closeEvent(QCloseEvent* event)
{
  deleteLater();
  QMainWindow::closeEvent(event);
}

AdvancedSearch::Criteria AdvancedSearchWindow::generate_criteria() const
{
  AdvancedSearch::Criteria cr;

  QString name = ui->nameEdit->text();
  if (!name.isEmpty())
    cr[AdvancedSearch::Name] = name;

  QVariant type = ui->typeCombo->currentData();
  if (!type.isNull())
    cr[AdvancedSearch::Type] = type;

  return cr;
}
