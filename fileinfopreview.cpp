#include "fileinfopreview.h"
#include "ui_fileinfopreview.h"
#include "utils.h"
#include <QMimeDatabase>
#include <cstdio>
#include <ctime>
#include <grp.h>
#include <hawk/Filesystem.h>
#include <pwd.h>

FileInfoPreview::FileInfoPreview(QWidget* parent)
    : QWidget(parent), ui(new Ui::FileInfoPreview)
{
  ui->setupUi(this);
}

FileInfoPreview::~FileInfoPreview() { delete ui; }

void FileInfoPreview::set_path(const hawk::Path& p)
{
  hawk::Stat st = hawk::symlink_status(p);
  QMimeDatabase mdb;

  auto perm = [&](unsigned mask, char c) {
    return (st.st_mode & mask) ? c : '-';
  };

  char buf[80];
  ui->sizeLabel->setText(human_readable_filesize(st.st_size, buf));
  ui->mimeLabel->setText(mdb.mimeTypeForFile(p.c_str()).name());

  time_t t = st.st_mtime;
  struct tm lt;
  localtime_r(&t, &lt);
  strftime(buf, sizeof(buf), "%c", &lt);
  ui->modifiedLabel->setText(buf);

  passwd* pw = getpwuid(st.st_uid);
  group* gr = getgrgid(st.st_gid);

  sprintf(buf, "%c%c%c%c%c%c%c%c%c", perm(S_IRUSR, 'r'), perm(S_IWUSR, 'w'),
          perm(S_IXUSR, 'x'), perm(S_IRGRP, 'r'), perm(S_IWGRP, 'w'),
          perm(S_IXGRP, 'x'), perm(S_IROTH, 'r'), perm(S_IWOTH, 'w'),
          perm(S_IXOTH, 'x'));
  ui->permissionsLabel->setText(buf);

  if (pw)
    ui->ownerLabel->setText(pw->pw_name);

  if (gr)
    ui->groupLabel->setText(gr->gr_name);
}
