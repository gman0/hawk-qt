#include "filter.h"

bool Filter_none::operator()(const dirent64*) const noexcept { return true; }

bool Filter_hidden::operator()(const dirent64* ent) const noexcept
{
  return ent->d_name[0] != '.';
}

bool Filter_ndirectories::operator()(const dirent64* ent) const noexcept
{
  return hawk::is_directory(ent->d_type);
}

bool Filter_files_by_name::operator()(const dirent64* ent) const noexcept
{
  return hawk::is_directory(ent->d_type) ||
         QString(ent->d_name).contains(pattern, Qt::CaseInsensitive);
}

bool Filter_files_by_name_regex::operator()(const dirent64* ent) const noexcept
{
  return hawk::is_directory(ent->d_type) || rx.exactMatch(ent->d_name);
}
