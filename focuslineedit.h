#ifndef FOCUSLINEEDIT_H
#define FOCUSLINEEDIT_H

#include <QLineEdit>

class FocusLineEdit : public QLineEdit {
private:
  Q_OBJECT

public:
  FocusLineEdit(QWidget* parent = nullptr) : QLineEdit{parent} {}

protected:
  void focusInEvent(QFocusEvent* event) override;
  void focusOutEvent(QFocusEvent* event) override;

signals:
  void focusAcquired();
  void focusLost();
};

#endif // FOCUSLINEEDIT_H
