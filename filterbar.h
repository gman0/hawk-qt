#ifndef FILTERBAR_H
#define FILTERBAR_H

#include <QWidget>

class FilterBar : public QWidget {
private:
  Q_OBJECT

public:
  explicit FilterBar(QWidget* parent = nullptr);

signals:
  void filterChanged(const QString& filter);
  void focusLost();
};

#endif // FILTERBAR_H
