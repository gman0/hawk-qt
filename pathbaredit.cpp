#include "pathbaredit.h"
#include <QAbstractItemView>
#include <QCompleter>
#include <QKeyEvent>
#include <QScrollBar>

#include <QDebug>

#include <iostream>

PathBarEdit::PathBarEdit(QWidget* parent)
    : FocusLineEdit{parent}, m_completer{nullptr}
{
}

void PathBarEdit::setCompleter(QCompleter* completer)
{
  if (m_completer)
    disconnect(m_completer, nullptr, this, nullptr);

  m_completer = completer;
  if (!completer)
    return;

  m_completer->setWidget(this);
  m_completer->setCompletionMode(QCompleter::PopupCompletion);
  m_completer->setCaseSensitivity(Qt::CaseInsensitive);
  m_completer->setFilterMode(Qt::MatchContains);
  connect(m_completer, static_cast<void (QCompleter::*)(const QString&)>(
                           &QCompleter::activated),
          this, &PathBarEdit::insertCompletion);
}

QCompleter* PathBarEdit::completer() const { return m_completer; }

void PathBarEdit::set_path(const hawk::Path& dirpath)
{
  setText(dirpath.string().c_str());
}

void PathBarEdit::keyPressEvent(QKeyEvent* event)
{
  if (m_completer && m_completer->popup()->isVisible()) {
    switch (event->key()) {
    case Qt::Key_Enter:
    case Qt::Key_Return:
    case Qt::Key_Escape:
    case Qt::Key_Tab:
    case Qt::Key_Backtab:
      event->ignore();
      return;

    default:
      break;
    }
  }

  bool is_shortcut =
      ((event->modifiers() & Qt::ControlModifier) && event->key() == Qt::Key_E);
  if (!m_completer || !is_shortcut)
    FocusLineEdit::keyPressEvent(event);

  const bool ctrl_or_shift =
      event->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
  if (!m_completer || (ctrl_or_shift && event->text().isEmpty())) {
    m_completer->popup()->hide();
    return;
  }

  if (event->key() == Qt::Key_Slash) {
    QString t = text();
    if (!t.isEmpty()) {
      static_cast<PathCompletionModel*>(m_completer->model())
          ->set_path(text().toStdString());
    }
  }

  QString prefix = completionPrefix();
  if (prefix != m_completer->completionPrefix()) {
    m_completer->setCompletionPrefix(prefix);
    m_completer->popup()->setCurrentIndex(
        m_completer->completionModel()->index(0, 0));
  }

  QRect cr = cursorRect();
  cr.setWidth(m_completer->popup()->sizeHintForColumn(0) +
              m_completer->popup()->verticalScrollBar()->sizeHint().width());

  m_completer->complete(cr);
}

void PathBarEdit::focusInEvent(QFocusEvent* event)
{
  if (m_completer)
    m_completer->setWidget(this);

  FocusLineEdit::focusInEvent(event);
}

void PathBarEdit::focusOutEvent(QFocusEvent* event)
{
  if (m_completer)
    m_completer->popup()->hide();

  FocusLineEdit::focusOutEvent(event);
}

void PathBarEdit::insertCompletion(const QString& completion)
{
  if (m_completer->widget() != this)
    return;

  QString t = text();
  int extra = completion.length() - m_completer->completionPrefix().length();
  insert(completion.right(extra));
}

QString PathBarEdit::completionPrefix()
{
  QString t = text();
  int found = t.lastIndexOf('/') + 1;

  return t.mid(found);
}
