#include "directorywidgetdata.h"
#include "errorhandling.h"

DirectoryWidgetData::DirectoryWidgetData(hawk::Watchdog& w, Cache& c,
                                         CursorCache& cc,
                                         hawk::Tasking* tasking,
                                         hawk::Tasking::Exception_handler eh,
                                         QAbstractItemView* parent)
    : ts{tasking}, wd{w}, cache{c}, cursors{cc}, filters_id{0},
      selmodel{&model}, wspinner{parent}
{
  if (!ts) {
    ts = new hawk::Tasking(std::chrono::milliseconds{500}, std::move(eh));
    _owns_tasking = true;
  }
  else
    _owns_tasking = false;
}

DirectoryWidgetData::~DirectoryWidgetData()
{
  if (_owns_tasking)
    delete ts;
}

hawk::Path DirectoryWidgetData::get_dirpath() const
{
  std::lock_guard<std::mutex> lk{m};
  return dirpath;
}

void DirectoryWidgetData::release()
{
  std::lock_guard<std::mutex> lk{m};
  model.updateDirectory({});

  ts->run_blocking(std::make_unique<Empty_task>(TaskPriorty::interrupt));

  wd.remove_path(dirpath);
  dirpath.clear();
  page = 0;
}
