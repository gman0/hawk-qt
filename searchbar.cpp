#include "searchbar.h"
#include "advancedsearchwindow.h"
#include "columngroup.h"
#include "focuslineedit.h"
#include <QHBoxLayout>
#include <QPushButton>

SearchBar::SearchBar(ColumnGroup* cg, QWidget* parent)
    : QWidget{parent}, m_cg{cg}
{
  QHBoxLayout* layout = new QHBoxLayout();
  layout->setContentsMargins(0, 0, 0, 0);

  FocusLineEdit* edit = new FocusLineEdit(this);
  edit->setClearButtonEnabled(true);
  edit->setPlaceholderText("Search by name (text between / and / is "
                           "interpreted as a regular expression)");

  QPushButton* btn = new QPushButton(this);
  btn->setText("Advanced");

  layout->addWidget(edit);
  layout->addWidget(btn);

  setLayout(layout);

  connect(edit, &FocusLineEdit::returnPressed, [edit, cg, this] {
    //    emit searchChanged(edit->text());
    QString str = edit->text();

    AdvancedSearch::Criteria cr;
    cr[AdvancedSearch::MaxDepth] = 1;

    if (str.startsWith('/') && str.endsWith('/') && str.length() > 2)
      cr[AdvancedSearch::RegExp] = str.mid(1, str.length() - 2);
    else
      cr[AdvancedSearch::Name] = str;

    emit advancedSearch(cg->dirpath(), cr);
  });
  connect(edit, &FocusLineEdit::focusLost, [this] { emit focusLost(); });
  connect(btn, &QPushButton::clicked, [this] { show_advanced_search(); });
}

void SearchBar::show_advanced_search()
{
  AdvancedSearchWindow* w = new AdvancedSearchWindow(m_cg->dirpath(), this);

  connect(w, &AdvancedSearchWindow::search_for,
          [this](const hawk::Path& base, const AdvancedSearch::Criteria& cr) {
            emit advancedSearch(base, cr);
          });
  w->show();
}
